﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Servicios
{
    public class Encriptador
    {
        public static string MD5(string pass)
        {
            try
            {
                UnicodeEncoding UeCodigo = new UnicodeEncoding();
                byte[] ByteSourceText = UeCodigo.GetBytes(pass);
                MD5CryptoServiceProvider Md5 = new MD5CryptoServiceProvider();
                byte[] ByteHash = Md5.ComputeHash(ByteSourceText);
                return Convert.ToBase64String(ByteHash);
            }
            catch (CryptographicException ex) //atrapo excepcion del cifrado
            {
                throw (ex);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
