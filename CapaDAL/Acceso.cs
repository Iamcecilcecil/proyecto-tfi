﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace CapaDAL
{
    public class Acceso
    {
        private SqlConnection oConnection = new SqlConnection("Data Source =.\\sqlexpress; Initial Catalog = GreenTech; Integrated Security = True");
        SqlCommand cmd; // para consultas
        SqlTransaction oTransaccion;

        public bool Escribir(string ConsultaSQL, ArrayList AParametros)
        {
            oConnection.Open();

            try
            {
                oTransaccion = oConnection.BeginTransaction(); // damos por comienzo la transaccion

                cmd = new SqlCommand(ConsultaSQL, oConnection, oTransaccion); //declaro esto para escribir la consulta en la bd
                cmd.CommandType = CommandType.StoredProcedure;

                if (AParametros != null)
                {
                    foreach (SqlParameter dato in AParametros)
                    {
                        cmd.Parameters.AddWithValue(dato.ParameterName, dato.Value);
                    }
                }

                int respuesta = cmd.ExecuteNonQuery();
                oTransaccion.Commit(); // confirmamos la tx
                return true;
            }
            catch (SqlException ex)
            {
                oTransaccion.Rollback(); // no se pudo realizar la tx... volvemos al estado anterior de la bd
                throw ex;
            }
            catch (Exception ex)
            {
                oTransaccion.Rollback();
                throw ex;
            }

            finally
            {
                oConnection.Close();
            }
        }

        public SqlCommand CreateCommand(string nombre, ArrayList AParametros)
        {
            SqlCommand comando = new SqlCommand(nombre, oConnection);
            comando.CommandType = CommandType.StoredProcedure;
            if (AParametros != null)
            {
                comando.Parameters.AddRange(AParametros.ToArray());
            }

            return comando;
        }
        SqlConnection conexion;
        public void Open(string nameConfig)
        {
            conexion = new SqlConnection();
            //conexion.ConnectionString = ConfigurationManager.ConnectionStrings[nameConfig].ConnectionString;
            conexion.ConnectionString = @"Data Source=.\sqlexpress; Initial Catalog= master; Trusted_Connection=true";
            conexion.Open();
        }
        public SqlCommand CreateCommand(string nombre, List<SqlParameter> parametros)
        {
            SqlCommand comando = new SqlCommand(nombre, conexion);
            comando.CommandType = CommandType.StoredProcedure;
            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }

            return comando;
        }

        public bool Write(string ConsultSQL)
        {

            if (oConnection.State == ConnectionState.Closed)
            {
                oConnection.ConnectionString = (ConfigurationManager.ConnectionStrings["ZapateriaConnectionString"].ToString());
                oConnection.Open();
            }
            try
            {
                cmd = new SqlCommand(ConsultSQL, oConnection);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (SqlException ex)
            {
                return false;
                throw ex;
            }
            finally
            {
                oConnection.Close();
            }
        }

        public void Close()
        {
            conexion.Close();
            conexion.Dispose();
            conexion = null;
            GC.Collect();
        }

        public bool Escribir2(string ConsultaSQL, ArrayList AParametros)
        {
            oConnection.Open();
            int filas = 0;
            SqlCommand comando = CreateCommand(ConsultaSQL, AParametros);
            try
            {
                filas = comando.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                filas = -1;
                oConnection.Close();
                throw new Exception();
            }
            oConnection.Close();
            return Convert.ToBoolean(filas);
        }

        public bool LeerEscalar(string ConsultaSQL, ArrayList AParametros)
        {
            oConnection.Open();
            cmd = new SqlCommand(ConsultaSQL, oConnection);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {


                if (AParametros != null)
                {
                    foreach (SqlParameter dato in AParametros)
                    {
                        cmd.Parameters.AddWithValue(dato.ParameterName, dato.Value);
                    }
                }

                int Respuesta = Convert.ToInt32(cmd.ExecuteScalar());
                oConnection.Close();

                if (Respuesta > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable Leer(string consulta, ArrayList Aparametros)

        {
            DataTable tabla = new DataTable(); //necesito recuperar una tabla de la BD
                                               //la tabla que quiero recuperar va a corresponder a una consulta

            SqlDataAdapter Da;
            cmd = new SqlCommand(consulta, oConnection);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = consulta;

            try
            {
                Da = new SqlDataAdapter(cmd);

                if (Aparametros != null)
                {
                    foreach (SqlParameter dato in Aparametros)
                    {
                        cmd.Parameters.AddWithValue(dato.ParameterName, dato.Value);
                    }
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally// se ejecuta siempre el cierre de la conexion
            {

                oConnection.Close();
            }

            Da.Fill(tabla);
            return tabla; //devuelvo la tabla que recupere de SLQ
        }

    }
}
