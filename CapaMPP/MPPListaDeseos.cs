﻿using CapaBE;
using CapaDAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaMPP
{
    public class MPPListaDeseos
    {

        ArrayList ArraydeParametros;
        string ConsultaSQL;

        public int GenerarNroCarrito()
        {
            try
            {
                ArraydeParametros = new ArrayList(); // lo uso para los parametros que voy a necesitar para este store
                ConsultaSQL = "S_ListaDeseos_GenerarID";

                Acceso oDatos = new Acceso();
                //el return no va para digitos
                oDatos.Escribir(ConsultaSQL, ArraydeParametros);
                int id = LeerId();
                return id;
                
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }
        int id;
        public int LeerId()
        {
            try
            {
                ArraydeParametros = new ArrayList(); // lo uso para los parametros que voy a necesitar para este store
                ConsultaSQL = "S_ListaDeseos_RecuperarID";

                Acceso oDatos = new Acceso();
                DataTable Tabla;
                Tabla = oDatos.Leer(ConsultaSQL, ArraydeParametros);


               

                if (Tabla.Rows.Count > 0)
                {
                    foreach (DataRow Fila in Tabla.Rows)
                    {
                        var id_lista = Convert.ToInt32(Fila[0]);

                        id = id_lista;
                    }

                }

                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GuardarProductos(int id, Dictionary<int, int> prods)
        {
            try
            {
                ConsultaSQL = "S_ListaDeseos_AgregarProductos";

                foreach(var par in prods)
                {
                    int idProd = par.Key;
                    int cant = par.Value;

                    ArraydeParametros = new ArrayList(); // lo uso para los parametros que voy a necesitar para este store

                    SqlParameter Parametro1 = new SqlParameter();
                    Parametro1.ParameterName = "@Id_ListaDeseos";
                    Parametro1.Value = id;
                    Parametro1.SqlDbType = SqlDbType.Int;
                    ArraydeParametros.Add(Parametro1);

                    SqlParameter Parametro2 = new SqlParameter();
                    Parametro2.ParameterName = "@Id_Producto";
                    Parametro2.Value = idProd;
                    Parametro2.SqlDbType = SqlDbType.Int;
                    ArraydeParametros.Add(Parametro2);
                    

                    SqlParameter Parametro3 = new SqlParameter();
                    Parametro3.ParameterName = "@Cantidad";
                    Parametro3.Value = cant;
                    Parametro3.SqlDbType = SqlDbType.Int;
                    ArraydeParametros.Add(Parametro3);

                    Acceso oDatos = new Acceso();
                    oDatos.Escribir(ConsultaSQL, ArraydeParametros);
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
