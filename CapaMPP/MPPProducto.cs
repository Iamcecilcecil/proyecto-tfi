﻿using CapaBE;
using CapaDAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaMPP
{
    public class MPPProducto
    {
        ArrayList ArraydeParametros;
        string ConsultaSQL;

        public bool Alta(BEProducto oBEProducto)
        {
            try
            {
                ArraydeParametros = new ArrayList(); // lo uso para los parametros que voy a necesitar para este store
                ConsultaSQL = "S_Producto_Alta";

                SqlParameter Parametro1 = new SqlParameter();
                Parametro1.ParameterName = "@Nombre";
                Parametro1.Value = oBEProducto.Nombre;
                Parametro1.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro1);

                SqlParameter Parametro2 = new SqlParameter();
                Parametro2.ParameterName = "@Precio";
                Parametro2.Value = oBEProducto.Precio;
                Parametro2.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro2);

                SqlParameter Parametro3 = new SqlParameter();
                Parametro3.ParameterName = "@Descripcion";
                Parametro3.Value = oBEProducto.Descripcion;
                Parametro3.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro3);

                SqlParameter Parametro4 = new SqlParameter();
                Parametro4.ParameterName = "@Categoria";
                Parametro4.Value = oBEProducto.Categoria;
                Parametro4.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro4);

                SqlParameter Parametro5 = new SqlParameter();
                Parametro5.ParameterName = "@Stock";
                Parametro5.Value = oBEProducto.Stock;
                Parametro5.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro5);

                Acceso oDatos = new Acceso();
                oDatos.Escribir(ConsultaSQL, ArraydeParametros);
                return Convert.ToBoolean(1);
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<BEProducto> BuscarProducto(string prod)
        {
            
            try
            {
                ArraydeParametros = new ArrayList(); // lo uso para los parametros que voy a necesitar para este store
                ConsultaSQL = "S_Producto_Buscar";


                SqlParameter Parametro1 = new SqlParameter();
                Parametro1.ParameterName = "@Palabra";
                Parametro1.Value = prod;
                Parametro1.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro1);


                Acceso oDatos = new Acceso();
                DataTable Tabla;
                Tabla = oDatos.Leer(ConsultaSQL, ArraydeParametros);


                List<BEProducto> listaprodu = new List<BEProducto>();    

                if (Tabla.Rows.Count > 0)
                {
                    foreach (DataRow Fila in Tabla.Rows)
                   {
                        var id = Convert.ToInt32(Fila[0]);
                        var nombre = Fila[1].ToString();
                        var precio = Convert.ToInt32(Fila[2]);
                        var descripcion= Fila[3].ToString();
                        var categoria= Fila[4].ToString();
                        var stock= Convert.ToInt32((int)Fila[5]);
                        var imagen = Fila[6].ToString();
                        var baja = Convert.ToInt32(Fila[7]);

                        BEProducto oProd = new BEProducto();

                        oProd.ID = id;
                        oProd.Nombre=nombre;
                        oProd.Precio=precio;
                        oProd.Descripcion=descripcion;
                        oProd.Categoria=categoria;
                        oProd.Stock=stock;
                        oProd.Imagen = imagen;


                        listaprodu.Add(oProd);
                    }
                
                }

                return listaprodu;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<BEProducto> Leer()
        {
            try
            {
                ArraydeParametros = new ArrayList(); // lo uso para los parametros que voy a necesitar para este store
                ConsultaSQL = "S_Producto_Leer";

                Acceso oDatos = new Acceso();
                DataTable Tabla;
                Tabla = oDatos.Leer(ConsultaSQL, ArraydeParametros);

                
                IList<BEProducto> lista= new List<BEProducto>();    

                if (Tabla.Rows.Count > 0)
                {
                    foreach (DataRow Fila in Tabla.Rows)
                   {
                        var id = Convert.ToInt32(Fila[0]);
                        var nombre = Fila[1].ToString();
                        var precio = Convert.ToInt32(Fila[2]);
                        var descripcion= Fila[3].ToString();
                        var categoria= Fila[4].ToString();
                        var stock= Convert.ToInt32((int)Fila[5]);
                        var imagen = Fila[6].ToString();
                        var baja = Convert.ToInt32(Fila[7]);

                        BEProducto oProd = new BEProducto();

                        oProd.ID = id;
                        oProd.Nombre=nombre;
                        oProd.Precio=precio;
                        oProd.Descripcion=descripcion;
                        oProd.Categoria=categoria;
                        oProd.Stock=stock;
                        oProd.Imagen = imagen;


                        lista.Add(oProd);
                    }
                
                }

                return lista;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Modificar(BEProducto oBEProducto)
        {
            try
            {
                ArraydeParametros = new ArrayList(); // lo uso para los parametros que voy a necesitar para este store
                ConsultaSQL = "S_Producto_Modificar";

                SqlParameter Parametro1 = new SqlParameter();
                Parametro1.ParameterName = "@Id_Producto";
                Parametro1.Value = oBEProducto.ID;
                Parametro1.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro1);

                SqlParameter Parametro2 = new SqlParameter();
                Parametro2.ParameterName = "@Nombre";
                Parametro2.Value = oBEProducto.Nombre;
                Parametro2.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro2);

                SqlParameter Parametro3 = new SqlParameter();
                Parametro3.ParameterName = "@Precio";
                Parametro3.Value = oBEProducto.Precio;
                Parametro3.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro3);

                SqlParameter Parametro4 = new SqlParameter();
                Parametro4.ParameterName = "@Descripcion";
                Parametro4.Value = oBEProducto.Descripcion;
                Parametro4.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro4);

                SqlParameter Parametro6 = new SqlParameter();
                Parametro6.ParameterName = "@Categoria";
                Parametro6.Value = oBEProducto.Categoria;
                Parametro6.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro6);

                SqlParameter Parametro5 = new SqlParameter();
                Parametro5.ParameterName = "@Stock";
                Parametro5.Value = oBEProducto.Stock;
                Parametro5.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro5);

                Acceso oDatos = new Acceso();
                oDatos.Escribir(ConsultaSQL, ArraydeParametros);
                return Convert.ToBoolean(1);
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void SubirImagen(string path, int Id)
        {
            try
            {
                ArraydeParametros = new ArrayList(); // lo uso para los parametros que voy a necesitar para este store
                ConsultaSQL = "S_Producto_SubirImagen";

                SqlParameter Parametro0 = new SqlParameter();
                Parametro0.ParameterName = "@Id";
                Parametro0.Value = Id;
                Parametro0.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro0);

                SqlParameter Parametro1 = new SqlParameter();
                Parametro1.ParameterName = "@Imagen";
                Parametro1.Value = path;
                Parametro1.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro1);

                
                Acceso oDatos = new Acceso();
                oDatos.Escribir(ConsultaSQL, ArraydeParametros);

            }

            catch (Exception ex)
            {
                throw ex;
            }

        }
        public bool VerificarExistencia(BEProducto oBEProducto)
        {
            try
            {
                Acceso oDatos = new Acceso();
                ArraydeParametros = new ArrayList();
                ConsultaSQL = "S_Producto_VerificarExistente";

                SqlParameter Parametro0 = new SqlParameter();
                Parametro0.ParameterName = "@Id_Producto";
                Parametro0.Value = oBEProducto.ID;
                Parametro0.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro0);

                SqlParameter Parametro1 = new SqlParameter();
                Parametro1.ParameterName = "@Nombre";
                Parametro1.Value = oBEProducto.Nombre;
                Parametro1.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro1);

                SqlParameter Parametro2 = new SqlParameter();
                Parametro2.ParameterName = "@Precio";
                Parametro2.Value = oBEProducto.Precio;
                Parametro2.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro2);

                SqlParameter Parametro3 = new SqlParameter();
                Parametro3.ParameterName = "@Descripcion";
                Parametro3.Value = oBEProducto.Descripcion;
                Parametro3.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro3);

                SqlParameter Parametro4 = new SqlParameter();
                Parametro4.ParameterName = "@Categoria";
                Parametro4.Value = oBEProducto.Categoria;
                Parametro4.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro4);

                SqlParameter Parametro5 = new SqlParameter();
                Parametro5.ParameterName = "@Stock";
                Parametro5.Value = oBEProducto.Stock;
                Parametro5.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro5);

                return oDatos.LeerEscalar(ConsultaSQL, ArraydeParametros);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Baja(BEProducto oBEProducto)
        {
            try
            {
                ArraydeParametros = new ArrayList();
                ConsultaSQL = "S_Producto_Baja";


                SqlParameter Parametro1 = new SqlParameter();
                Parametro1.ParameterName = "@Id_Producto";
                Parametro1.Value = oBEProducto.ID;
                Parametro1.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro1);

                Acceso oDatos = new Acceso();
                oDatos.Escribir(ConsultaSQL, ArraydeParametros);
                bool ok = oDatos.Escribir(ConsultaSQL, ArraydeParametros);

                return ok;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int ObtenerID(BEProducto oBEProducto)
        {
            try
            {
                ArraydeParametros = new ArrayList(); // lo uso para los parametros que voy a necesitar para este store
                ConsultaSQL = "S_Producto_ObtenerID";

                SqlParameter Parametro1 = new SqlParameter();
                Parametro1.ParameterName = "@Nombre";
                Parametro1.Value = oBEProducto.Nombre;
                Parametro1.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro1);

                SqlParameter Parametro2 = new SqlParameter();
                Parametro2.ParameterName = "@Precio";
                Parametro2.Value = oBEProducto.Precio;
                Parametro2.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro2);

                SqlParameter Parametro3 = new SqlParameter();
                Parametro3.ParameterName = "@Descripcion";
                Parametro3.Value = oBEProducto.Descripcion;
                Parametro3.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro3);

                SqlParameter Parametro4 = new SqlParameter();
                Parametro4.ParameterName = "@Categoria";
                Parametro4.Value = oBEProducto.Categoria;
                Parametro4.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro4);

                SqlParameter Parametro5 = new SqlParameter();
                Parametro5.ParameterName = "@Stock";
                Parametro5.Value = oBEProducto.Stock;
                Parametro5.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro5);

                Acceso oDatos = new Acceso();
                DataTable Tabla;
                Tabla= oDatos.Leer(ConsultaSQL, ArraydeParametros);
               
                BEProducto oProd= new BEProducto();

                if(Tabla.Rows.Count>0)
                {
                    foreach(DataRow Fila in Tabla.Rows)
                    {
                        var id = Convert.ToInt32(Fila[0]);
                        oProd.ID = id;
                    }
                   
                }

                return oProd.ID;
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
