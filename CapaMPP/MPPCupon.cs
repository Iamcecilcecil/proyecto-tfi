﻿using CapaBE;
using CapaDAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaMPP
{
    public class MPPCupon
    {

        ArrayList ArraydeParametros;
        string ConsultaSQL;

        public bool Alta(BECupon oBECupon)
        {
            try
            {
                ArraydeParametros = new ArrayList(); // lo uso para los parametros que voy a necesitar para este store
                ConsultaSQL = "S_Cupon_Alta";

                SqlParameter Parametro1 = new SqlParameter();
                Parametro1.ParameterName = "@Codigo";
                Parametro1.Value = oBECupon.Codigo;
                Parametro1.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro1);

                SqlParameter Parametro2 = new SqlParameter();
                Parametro2.ParameterName = "@Estado";
                Parametro2.Value = oBECupon.Estado;
                Parametro2.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro2);

                SqlParameter Parametro3 = new SqlParameter();
                Parametro3.ParameterName = "@Fecha";
                Parametro3.Value = oBECupon.Fecha_Vencimiento;
                Parametro3.SqlDbType = SqlDbType.Date;
                ArraydeParametros.Add(Parametro3);

                SqlParameter Parametro4 = new SqlParameter();
                Parametro4.ParameterName = "@Porcentaje";
                Parametro4.Value = oBECupon.Porcentaje_Descuento;
                Parametro4.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro4);

                Acceso oDatos = new Acceso();
                //el return no va para digitos
                oDatos.Escribir(ConsultaSQL, ArraydeParametros);
                return Convert.ToBoolean(1);
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }

        public IList<BECupon> Leer()
        {
            try
            {
                ArraydeParametros = new ArrayList(); // lo uso para los parametros que voy a necesitar para este store
                ConsultaSQL = "S_Cupon_Leer";

                Acceso oDatos = new Acceso();
                DataTable Tabla;
                Tabla = oDatos.Leer(ConsultaSQL, ArraydeParametros);


                IList<BECupon> listaCupones = new List<BECupon>();

                if (Tabla.Rows.Count > 0)
                {
                    foreach (DataRow Fila in Tabla.Rows)
                    {
                        var id = Convert.ToInt32(Fila[0]);
                        var codigo = Fila[1].ToString();
                        var estado = Fila[2].ToString();
                        var fecha_vencimiento = Convert.ToDateTime(Fila[3]);
                        var porcentaje_descuento = Convert.ToInt32(Fila[4]);
                        var baja = Convert.ToInt32(Fila[5]);

                        BECupon oCupon = new BECupon();

                        oCupon.ID = id;
                        oCupon.Codigo = codigo;
                        oCupon.Estado = estado;
                        oCupon.Fecha_Vencimiento = fecha_vencimiento;
                        oCupon.Porcentaje_Descuento = porcentaje_descuento;

                        listaCupones.Add(oCupon);
                    }

                }

                return listaCupones;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Modificar(BECupon oBECupon)
        {
            try
            {
                ArraydeParametros = new ArrayList();
                ConsultaSQL = "S_Cupon_Modificar";

                SqlParameter Parametro1 = new SqlParameter();
                Parametro1.ParameterName = "@Id";
                Parametro1.Value = oBECupon.ID;
                Parametro1.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro1);

                SqlParameter Parametro2 = new SqlParameter();
                Parametro2.ParameterName = "@Codigo";
                Parametro2.Value = oBECupon.Codigo;
                Parametro2.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro2);

                SqlParameter Parametro3 = new SqlParameter();
                Parametro3.ParameterName = "@Estado";
                Parametro3.Value = oBECupon.Estado;
                Parametro3.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro3);

                SqlParameter Parametro4 = new SqlParameter();
                Parametro4.ParameterName = "@Fecha_vencimiento";
                Parametro4.Value = oBECupon.Fecha_Vencimiento;
                Parametro4.SqlDbType = SqlDbType.Date;
                ArraydeParametros.Add(Parametro4);

                SqlParameter Parametro5 = new SqlParameter();
                Parametro5.ParameterName = "@Porcentaje_descuento";
                Parametro5.Value = oBECupon.Porcentaje_Descuento;
                Parametro5.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro5);

                Acceso oDatos = new Acceso();
                bool ok = oDatos.Escribir(ConsultaSQL, ArraydeParametros);
                return ok;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool Baja(BECupon oBECupon)
        {
            try
            { 
                ArraydeParametros = new ArrayList();
                ConsultaSQL = "S_Cupon_Baja";
                

                SqlParameter Parametro1 = new SqlParameter();
                Parametro1.ParameterName = "@Id";
                Parametro1.Value = oBECupon.ID;
                Parametro1.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro1);

                Acceso oDatos = new Acceso();
                oDatos.Escribir(ConsultaSQL, ArraydeParametros);
                bool ok = oDatos.Escribir(ConsultaSQL, ArraydeParametros);

                return ok;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}