﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaBE;
using CapaDAL;

namespace CapaMPP
{
    public class MPPCliente
    {
        ArrayList ArraydeParametros;
        string ConsultaSQL;

        public bool Validacion_user_pass(BECliente oBECliente)
        {
            try
            {
                Acceso oDatos = new Acceso(); //s_usuario_validar_UC
                ArraydeParametros = new ArrayList();
                ConsultaSQL = "S_Usuario_Validar_UC";

                SqlParameter Parametro1 = new SqlParameter();
                Parametro1.ParameterName = "@Email";
                Parametro1.Value = oBECliente.Email;
                Parametro1.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro1);

                SqlParameter Parametro2 = new SqlParameter();
                Parametro2.ParameterName = "@Contrasenia";
                Parametro2.Value = oBECliente.Contraseñia;
                Parametro2.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro2);

                return oDatos.LeerEscalar(ConsultaSQL, ArraydeParametros);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ModificarContraseña(BECliente oBECliente)
        {
            try
            {
                ArraydeParametros = new ArrayList(); // lo uso para los parametros que voy a necesitar para este store
                ConsultaSQL = "S_Usuario_ModificarContraseña";

                SqlParameter Parametro1 = new SqlParameter();
                Parametro1.ParameterName = "@Usuario";
                Parametro1.Value = oBECliente.Email;
                Parametro1.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro1);

                SqlParameter Parametro2 = new SqlParameter();
                Parametro2.ParameterName = "@Contraseña";
                Parametro2.Value = oBECliente.Contraseñia;
                Parametro2.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro2);

                Acceso oDatos = new Acceso();
                oDatos.Escribir(ConsultaSQL, ArraydeParametros);
            }
            catch (Exception ex)
            {

            }
        }

        public bool Validacion_usuario(string usuario)
        {
            try
            {
                Acceso oDatos = new Acceso(); 
                ArraydeParametros = new ArrayList();
                ConsultaSQL = "S_Usuario_ValidarUsuario";

                SqlParameter Parametro1 = new SqlParameter();
                Parametro1.ParameterName = "@Usuario";
                Parametro1.Value = usuario;
                Parametro1.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro1);

                return oDatos.LeerEscalar(ConsultaSQL, ArraydeParametros);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Alta(BECliente oBECliente)
        {
            try
            {
                ArraydeParametros = new ArrayList(); // lo uso para los parametros que voy a necesitar para este store
                ConsultaSQL = "S_Usuario_Alta";

                SqlParameter Parametro1 = new SqlParameter();
                Parametro1.ParameterName = "@Email";
                Parametro1.Value = oBECliente.Email;
                Parametro1.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro1);

                SqlParameter Parametro2 = new SqlParameter();
                Parametro2.ParameterName = "@Contrasenia";
                Parametro2.Value = oBECliente.Contraseñia;
                Parametro2.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro2);

                SqlParameter Parametro5 = new SqlParameter();
                Parametro5.ParameterName = "@Nombre";
                Parametro5.Value = oBECliente.Nombre;
                Parametro5.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro5);

                SqlParameter Parametro6 = new SqlParameter();
                Parametro6.ParameterName = "@Apellido";
                Parametro6.Value = oBECliente.Apellido;
                Parametro6.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro6);

                SqlParameter Parametro7 = new SqlParameter();
                Parametro7.ParameterName = "@Telefono";
                Parametro7.Value = oBECliente.Telefono;
                Parametro7.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro7);

                SqlParameter Parametro8 = new SqlParameter();
                Parametro8.ParameterName = "@Permiso";
                Parametro8.Value = 0;
                Parametro8.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro8);

                Acceso oDatos = new Acceso();
                oDatos.Escribir(ConsultaSQL, ArraydeParametros);

                return 1;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BECliente ListarCliente(BECliente oBECliente)
        {
            Acceso oDatos = new Acceso();

            ArraydeParametros = new ArrayList();

            ConsultaSQL = "S_USUARIOS_GETUSER";
            BECliente cliente = new BECliente();


            SqlParameter Parametro1 = new SqlParameter();
            Parametro1.ParameterName = "@Email";
            Parametro1.Value = oBECliente.Email;
            Parametro1.SqlDbType = SqlDbType.VarChar;
            ArraydeParametros.Add(Parametro1);


            DataTable Tabla;

            Tabla = oDatos.Leer(ConsultaSQL, ArraydeParametros);

            foreach (DataRow Fila in Tabla.Rows)
            {

                cliente.ID = Convert.ToInt32(Fila[0]);
                cliente.Nombre = Fila[1].ToString();
                cliente.Apellido = Fila[2].ToString();
                cliente.Telefono = Convert.ToInt32(Fila[3]);
                cliente.Email = Fila[4].ToString();
                cliente.Contraseñia = Fila[5].ToString();
                cliente.Permiso = Convert.ToInt32(Fila[6]);

            }

            return cliente;
        }
    }

}
