﻿using CapaBE;
using CapaDAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaMPP
{
    public class MPPCompra
    {
        ArrayList ArraydeParametros;
        string ConsultaSQL;
        public IList<BECompra> Leer()
        {
           

            try
            {
                ArraydeParametros = new ArrayList(); // lo uso para los parametros que voy a necesitar para este store
                ConsultaSQL = "S_Compra_Leer";

                Acceso oDatos = new Acceso();
                DataTable Tabla;
                Tabla = oDatos.Leer(ConsultaSQL, ArraydeParametros);


                IList<BECompra> listaCompras = new List<BECompra>();

                if (Tabla.Rows.Count > 0)
                {
                    foreach (DataRow Fila in Tabla.Rows)
                    {
                        var id_compra = Convert.ToInt32(Fila[0]);
                        var total = Convert.ToInt32(Fila[1]);
                        var estado = Fila[2].ToString();
                        var comentario = Fila[3].ToString();
                        var direccion = Fila[4].ToString();
                        var altura = Convert.ToInt32(Fila[5]);
                        var pais = Fila[6].ToString();
                        var provincia = Fila[7].ToString();
                        var ciudad = Fila[8].ToString();
                        var CP = Convert.ToInt32(Fila[9]);
                        var idcliente = Convert.ToInt32(Fila[10]);

                        BECliente oCliente = new BECliente();

                        SqlParameter Parametro1 = new SqlParameter();
                        Parametro1.ParameterName = "@Id";
                        Parametro1.Value = idcliente;
                        Parametro1.SqlDbType = SqlDbType.Int;
                        ArraydeParametros.Add(Parametro1);

                        ConsultaSQL = "S_Cliente_Leer";
                      
                        DataTable Tabla2;
                        Tabla2 = oDatos.Leer(ConsultaSQL, ArraydeParametros);

                        foreach (DataRow Fila2 in Tabla2.Rows)
                        {

                            oCliente.ID = Convert.ToInt32(Fila2[0]);
                            oCliente.Nombre = Fila2[1].ToString();
                            oCliente.Apellido = Fila2[2].ToString();
                            oCliente.Telefono = Convert.ToInt32(Fila2[3]);
                            oCliente.Email= Fila2[4].ToString();
                        }

                        BECompra oCompra = new BECompra();

                        oCompra.ID = id_compra;
                        oCompra.Total = total;
                        oCompra.Estado= estado;
                        oCompra.Comentario= comentario;
                        oCompra.Direccion= direccion;
                        oCompra.Altura = altura;
                        oCompra.Pais = pais;
                        oCompra.Provincia = provincia;
                        oCompra.Ciudad = ciudad;
                        oCompra.CP = CP;
                        oCompra.Cliente = oCliente;
                 
                        listaCompras.Add(oCompra);
                    }

                }

                return listaCompras;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ModificarEstado(BECompra oBECompra)
        {
            try
            {
                Acceso oDatos = new Acceso(); //s_usuario_validar_UC
                ArraydeParametros = new ArrayList();
                ConsultaSQL = "S_Compra_ModificarEstado";

                SqlParameter Parametro1 = new SqlParameter();
                Parametro1.ParameterName = "@Id_Compra";
                Parametro1.Value = oBECompra.ID;
                Parametro1.SqlDbType = SqlDbType.Int;
                ArraydeParametros.Add(Parametro1);

                SqlParameter Parametro2 = new SqlParameter();
                Parametro2.ParameterName = "@Estado";
                Parametro2.Value = oBECompra.Estado;
                Parametro2.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro2);

                SqlParameter Parametro3 = new SqlParameter();
                Parametro3.ParameterName = "@Comentario";
                Parametro3.Value = oBECompra.Comentario;
                Parametro3.SqlDbType = SqlDbType.VarChar;
                ArraydeParametros.Add(Parametro3);

               
                bool ok = oDatos.Escribir(ConsultaSQL, ArraydeParametros);
                return ok;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
