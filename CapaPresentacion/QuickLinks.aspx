﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="QuickLinks.aspx.cs" Inherits="CapaPresentacion.QuickLinks" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Quick Links</h1>
     <br/>
        <asp:HyperLink ID="HyperLink" runat="server" NavigateUrl="~/PreguntasFrecuentes.aspx" Font-Size="Large">Preguntas Frecuentes</asp:HyperLink>
    <br/>
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/ComoComprar.aspx" Font-Size="Large">Como comprar</asp:HyperLink>
    <br/>
        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/FormasPago.aspx" Font-Size="Large">Medios de pago</asp:HyperLink>
    <br/>
        <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/CambiosDevoluciones.aspx" Font-Size="Large">Cambios y devoluciones</asp:HyperLink>
    <br/>
        <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Politicas.aspx" Font-Size="Large">Politicas de privacidad</asp:HyperLink>
    <br/>
        <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/SobreNosotros.aspx" Font-Size="Large">¿Quienes somos?</asp:HyperLink>
    <asp:Image ID="Image1" runat="server" Height="300px" ImageUrl="~/img/preguntas.png" Width="300px" ImageAlign="Right" Style="float:right; margin-right:10px; margin-top:-130px" />
</asp:Content>

