﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaBE;
using CapaBLL;


namespace CapaPresentacion
{
    public partial class AltaCupones : System.Web.UI.Page
    {
        BECupon oBECupon = new BECupon();
        BLLCupon oBLLCupon = new BLLCupon();
        List<BECupon> listacupones= new List<BECupon>();
        protected void Page_Load(object sender, EventArgs e)
        {
            string admin = Convert.ToString(Session["esadmin"]);
            if (admin == "1")
            {
                this.Page.Master.FindControl("admincup").Visible = true;
                this.Page.Master.FindControl("adminprod").Visible = true;
                this.Page.Master.FindControl("adminven").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("admincup").Visible = false;
                this.Page.Master.FindControl("adminprod").Visible = false;
                this.Page.Master.FindControl("adminven").Visible = false;
                Response.Redirect("Productos.aspx");
            }

            if (admin != "")
            {
                this.Page.Master.FindControl("inisesion").Visible = false;
                this.Page.Master.FindControl("cersesion").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("inisesion").Visible = true;
                this.Page.Master.FindControl("cersesion").Visible = false;
            }

        }

        protected void btnAlta_Click(object sender, EventArgs e)
        {
            if (txtEstado.Text != "" && txtFecha.Text!="" && txtPorcentaje.Text!="")
            {
                oBECupon.Codigo = txtCodigo.Text;
                oBECupon.Estado = txtEstado.Text;
                oBECupon.Fecha_Vencimiento = Convert.ToDateTime(txtFecha.Text);
                oBECupon.Porcentaje_Descuento = Convert.ToInt32(txtPorcentaje.Text);

                oBLLCupon.Alta(oBECupon);
                lblOk.Text = "Cupon dado de alta correctamente";
            }
            else
            {
                // no se seleccionó un cupon para dar de alta o faltan datos
            }
        }
    }
}