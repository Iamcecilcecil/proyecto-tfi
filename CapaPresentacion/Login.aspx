﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="CapaPresentacion.Log2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<body class="bg-black">
    <div class="form-box" id="login-box" style="margin-top:20px">
        <div class="header" style="background-color: #008080">Iniciar Sesion</div>
        <div class="body bg-gray">
            <div class="form-group">
                <asp:TextBox ID="txtUsuario" runat="server" CssClass="form-control" placeholder="Ingrese su usuario: " MaxLength="30"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtUsuario" forecolor="#df1154" ErrorMessage="Por favor, ingrese un email valido"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" forecolor="#df1154" ErrorMessage="Email invalido" ControlToValidate="txtUsuario" ValidationExpression="^([0-9A-Za-z.\-+]+@[0-9A-Za-z.\-]+\.[A-Za-z]{2,6})$"></asp:RegularExpressionValidator>
            </div>
            <div class="form-group">
                <asp:TextBox ID="txtContraseña" runat="server"  CssClass="form-control" placeholder="Ingrese su contraseña" MaxLength="10"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtContraseña" forecolor="#df1154" ErrorMessage="Por favor, ingrese una contraseña valida"></asp:RequiredFieldValidator>
           <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" forecolor="#df1154" ErrorMessage="Contraseña invalida" ControlToValidate="txtContraseña" ValidationExpression="^(?=(?:[^0-9]*[0-9]){2})[A-Za-z0-9]{10}$"></asp:RegularExpressionValidator>
                </div>
        </div>
        <div class="footer">
        <asp:Button ID="btnIniciarSesion" runat="server" Text="Iniciar Sesion" CssClass="btn bg-till btn-block" Height="50px" BackColor="#008080" OnClick="btnIniciarSesion_Click" />
        <asp:Label runat="server" ID="lblError" CssClass="alert-danger"></asp:Label>
            <br>
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/RecuperarContraseña.aspx">¿Olvidó su contraseña?</asp:HyperLink>
                        <br>
                        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Registro.aspx">Registrarse</asp:HyperLink>
        </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
</body>

</asp:Content>
