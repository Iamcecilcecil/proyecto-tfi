﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="AltaCupones.aspx.cs" Inherits="CapaPresentacion.AltaCupones" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<body class="bg-black">
    <div class="form-box" id="login-box" style="margin-top:20px">
        <div class="header" style="background-color: #008080">Alta de Cupones</div>
        <div class="body bg-gray">
            <div class="form-group">
                <asp:Label ID="lblCodigo" runat="server" Text="Codigo de Cupon"></asp:Label>
                <asp:TextBox ID="txtCodigo" runat="server" Enabled="false" CssClass="form-control" ></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblPorcentaje" runat="server" Text="Porcentaje de Descuento"></asp:Label>
                <asp:TextBox ID="txtPorcentaje" runat="server"  CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblFecha" runat="server" Text="Fecha de Vencimiento"></asp:Label>
                <asp:TextBox ID="txtFecha" runat="server"  CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblEstado" runat="server" Text="Estado de cupon"></asp:Label>
                <asp:TextBox ID="txtEstado" runat="server"  CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="footer">
        <asp:Button ID="btnAlta" runat="server" Text="Dar de Alta" CssClass="btn bg-till btn-block" Height="50px" BackColor="#008080" OnClick="btnAlta_Click" />
        <asp:Label runat="server" ID="lblError" CssClass="alert-danger"></asp:Label>
            <br />
        <asp:Label ID="lblOk" CssClass="alert-success" runat="server"></asp:Label>
            <br />
            
        </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
</body>

</asp:Content>
