﻿using CapaBE;
using CapaBLL;
using System;
using System.Collections.Generic;

namespace CapaPresentacion
{
    public partial class AdminCompras : System.Web.UI.Page
    {
        BECompra oBECompra = new BECompra();
        BLLCompra oBLLCompra = new BLLCompra();

        protected void Page_Load(object sender, EventArgs e)
        {
            string admin = Convert.ToString(Session["esadmin"]);
            if (admin == "1")
            {
                this.Page.Master.FindControl("admincup").Visible = true;
                this.Page.Master.FindControl("adminprod").Visible = true;
                this.Page.Master.FindControl("adminven").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("admincup").Visible = false;
                this.Page.Master.FindControl("adminprod").Visible = false;
                this.Page.Master.FindControl("adminven").Visible = false;
                Response.Redirect("Productos.aspx");
            }

            if (admin != "")
            {
                this.Page.Master.FindControl("inisesion").Visible = false;
                this.Page.Master.FindControl("cersesion").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("inisesion").Visible = true;
                this.Page.Master.FindControl("cersesion").Visible = false;
            }

            if (!IsPostBack)
            {
                Session["compra"] = null;
                GridView3.DataSource = oBLLCompra.Leer();
                GridView3.DataBind();
            }
            else
            {
                GridView3.DataSource = oBLLCompra.Leer();
                GridView3.DataBind();
            }
           
        }

        protected void GridView3_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<BECompra> listadecompras = new List<BECompra>();
            listadecompras = (List<BECompra>)oBLLCompra.Leer();
            BECliente oCli= new BECliente();

            oBECompra.ID = Convert.ToInt32(this.GridView3.Rows[GridView3.SelectedIndex].Cells[1].Text);
            oBECompra.Total= Convert.ToInt32(this.GridView3.Rows[GridView3.SelectedIndex].Cells[2].Text);
            oBECompra.Estado = this.GridView3.Rows[GridView3.SelectedIndex].Cells[3].Text;
            oBECompra.Comentario= this.GridView3.Rows[GridView3.SelectedIndex].Cells[4].Text;
            oBECompra.Direccion= this.GridView3.Rows[GridView3.SelectedIndex].Cells[5].Text;
            oBECompra.Altura = Convert.ToInt32(this.GridView3.Rows[GridView3.SelectedIndex].Cells[6].Text);
            oBECompra.Pais = this.GridView3.Rows[GridView3.SelectedIndex].Cells[7].Text;
            oBECompra.Provincia = this.GridView3.Rows[GridView3.SelectedIndex].Cells[8].Text;
            oBECompra.Ciudad = this.GridView3.Rows[GridView3.SelectedIndex].Cells[9].Text;
            oBECompra.CP= Convert.ToInt32(this.GridView3.Rows[GridView3.SelectedIndex].Cells[10].Text);
            
            foreach(BECompra compra in listadecompras)
            {
               
                if(compra.ID==oBECompra.ID)
                {

                        oCli = compra.Cliente;
                    
                   
                }
            }
            
            oBECompra.Cliente =oCli;
            
            Session["compra"] = oBECompra;
        
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            if (Session["compra"]!=null)
            {
                Response.Redirect("/EditarCompra.aspx");
            }
            else
            {
                //error no se seleccionó una compra
            }
           
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("Productos.aspx");
        }
    }
}