﻿using CapaBE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CapaBLL;

namespace CapaPresentacion
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        BLLProducto obllproducto = new BLLProducto();
        List<BEProducto> listaproductos;
        protected void Page_Load(object sender, EventArgs e)
        {
           

            string admin = Convert.ToString(Session["esadmin"]);
            if(admin=="1")
            {
                this.Page.Master.FindControl("admincup").Visible = true;
                this.Page.Master.FindControl("adminprod").Visible = true;
                this.Page.Master.FindControl("adminven").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("admincup").Visible = false;
                this.Page.Master.FindControl("adminprod").Visible = false;
                this.Page.Master.FindControl("adminven").Visible = false;
            }
            if (admin != "")
            {
                this.Page.Master.FindControl("inisesion").Visible = false;
                this.Page.Master.FindControl("cersesion").Visible = true;
            }else
            {
                this.Page.Master.FindControl("inisesion").Visible = true;
                this.Page.Master.FindControl("cersesion").Visible = false;
            }
            if (!IsPostBack)
            {
                // Solo inicializa la lista en la primera carga de la página
                if (Session["listaproductos"] != null)
                {
                    listaproductos = (List<BEProducto>)Session["listaproductos"];
                }
                else
                {
                    listaproductos = new List<BEProducto>();
                    Session["listaproductos"] = listaproductos;
                }
            }

            GridView1.DataSource = obllproducto.Leer();
            GridView1.DataBind();
          
          
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int f = GridView1.SelectedIndex;

            int codigo = Convert.ToInt32((GridView1.Rows[f].Cells[1].FindControl("lblCodigo") as Label).Text);
            string producto = ((GridView1.Rows[f].Cells[1].FindControl("lblNombre") as Label).Text);
            int precio = Convert.ToInt32((GridView1.Rows[f].Cells[1].FindControl("lblPrecio") as Label).Text);
            string descripcion = ((GridView1.Rows[f].Cells[1].FindControl("lblDesc") as Label).Text);
            string categoria = ((GridView1.Rows[f].Cells[1].FindControl("lblCat") as Label).Text);
            //guardar en variables de sesion y enviar a la pagina de ListaDeseos
            //ocultar imagen, stock y otras columnas
            BEProducto oBEProducto = new BEProducto();

            oBEProducto.ID = codigo;
            oBEProducto.Nombre = producto;
            oBEProducto.Descripcion = descripcion;
            oBEProducto.Categoria = categoria;
            oBEProducto.Precio = precio;
            
            if (Session["listaproductos"] != null)
            {
                listaproductos = (List<BEProducto>)Session["listaproductos"];
            }

            listaproductos.Add(oBEProducto);
            Session["listaproductos"] = listaproductos;

            Response.Redirect("ListaDeseos.aspx");
        }
    }
}