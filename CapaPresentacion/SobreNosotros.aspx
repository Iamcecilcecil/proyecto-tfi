﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="SobreNosotros.aspx.cs" Inherits="CapaPresentacion.SobreNosotros" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>¿Quienes somos?</h1>
    <br />
<div class="form-group">
        <asp:Label ID="lblRespuesta" runat="server" Text="GreenTech es una empresa lider en tecnologia de jardineria interior que revoluciona la forma en que las personas cultivan plantas en el entorno domestico. Con una combinacion unica de diseño elegante, tecnologia avanzada y simplicidad de uso, GreenTech ha logrado poner la jardineria al alcance de todos, independientemente del nivel de experiencia."></asp:Label>
             <br />
                <br />
    
    <asp:Image ID="Image2" runat="server" Height="261px" ImageUrl="~/img/harvest-elite-slim_Stainless_901124-0200_3x3.jpg" Width="239px" ImageAlign="Baseline" BorderStyle="None" />
    <asp:Image ID="Image1" runat="server" Height="261px" ImageUrl="~/img/901100-0200_806500-0208.jpeg" Width="239px" ImageAlign="Baseline" BorderStyle="None" />
    <asp:Image ID="Image3" runat="server" Height="261px" ImageUrl="~/img/bounty-basic_Black_903126-0100_3x3.jpg" Width="239px" ImageAlign="Baseline" BorderStyle="None" />
    </div>


</asp:Content>
