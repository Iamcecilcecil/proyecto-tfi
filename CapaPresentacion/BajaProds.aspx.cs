﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CapaPresentacion
{
    public partial class BajaProds : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string admin = Convert.ToString(Session["esadmin"]);
            if (admin == "1")
            {
                this.Page.Master.FindControl("admincup").Visible = true;
                this.Page.Master.FindControl("adminprod").Visible = true;
                this.Page.Master.FindControl("adminven").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("admincup").Visible = false;
                this.Page.Master.FindControl("adminprod").Visible = false;
                this.Page.Master.FindControl("adminven").Visible = false;
                Response.Redirect("Productos.aspx");
            }

            if (admin != "")
            {
                this.Page.Master.FindControl("inisesion").Visible = false;
                this.Page.Master.FindControl("cersesion").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("inisesion").Visible = true;
                this.Page.Master.FindControl("cersesion").Visible = false;
            }

            if (Page.IsPostBack == true)
            {

            }
            else
            {
                txtId.Text = Session["Id_producto"].ToString();
                txtNombre.Text = Session["Nombre_producto"].ToString();
                txtPrecio.Text = Session["Precio_producto"].ToString();
                txtDescripcion.Text = Session["Descripcion_producto"].ToString();
                txtCategoria.Text = Session["Categoria_producto"].ToString();
                txtStock.Text = Session["Stock_producto"].ToString();
                txtImagen.Text = Session["Imagen_producto"].ToString();
            }
        }

        protected void btnBaja_Click(object sender, EventArgs e)
        {
            Response.Redirect("/PreguntaBajaProds.aspx");
        }
    }
}