﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="Politicas.aspx.cs" Inherits="CapaPresentacion.Politicas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Politica de privacidad</h1>

<br />
<div class="form-group">
        <asp:Label ID="lblCookies" runat="server" Text="Eventualmente el sitio puede utilizar cookies, que se instalarán en la computadora del usuario cuando este navegue por el sitio. Una “cookie” es un fragmento de información que un servidor web puede colocar en la computadora del Usuario cuando el Usuario visita un sitio web. Las “cookies” tienen por finalidad facilitar la navegación por el sitio al Usuario, y proporcionar a GreenTech, información que le ayudara a mejorar los sus servicios y contenidos. En ningún caso las cookies utilizadas por el sitio proporcionarán información de carácter personal de Usuario, quien en relación a las mismas mantendrá pleno anonimato, aun frente a GreenTech, dado que tampoco suministran información tendiente a la individualización del Usuario.
Es intención de GreenTech poner de resalto que para navegar por el sitio, no resulta necesario que el Usuario permita la instalación de las cookies enviadas por el sitio. Ello solo podrá requerirse en relación a ciertos servicios y/o contenidos."></asp:Label>
             <br />
                <br />
    
    </div>
<h1>EN CUMPLIMIENTO DE LA DISPOSICION DE LA DNPDP N° 10/2008 SE INFORMA LO SIGUIENTE</h1>

<br />
<div class="form-group">
        <asp:Label ID="lblinforme" runat="server" Text="“El titular de los datos personales tiene la facultad de ejercer el derecho de acceso a los mismos en forma gratuita a intervalos no inferiores a seis meses, salvo que se acredite un interés legítimo al efecto conforme lo establecido en el artículo 14, inciso 3 de la Ley de Protección de Datos Personales”."></asp:Label>
             <br />
                <br />
    
    </div>


</asp:Content>
