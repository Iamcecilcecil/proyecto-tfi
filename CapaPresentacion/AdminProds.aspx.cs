﻿using CapaBLL;
using System;
using CapaBE;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Controls;

namespace CapaPresentacion
{
    public partial class AdminProds : System.Web.UI.Page
    {
        BLLProducto oBLLProducto = new BLLProducto();
        protected void Page_Load(object sender, EventArgs e)
        {
            string admin = Convert.ToString(Session["esadmin"]);
            if (admin == "1")
            {
                this.Page.Master.FindControl("admincup").Visible = true;
                this.Page.Master.FindControl("adminprod").Visible = true;
                this.Page.Master.FindControl("adminven").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("admincup").Visible = false;
                this.Page.Master.FindControl("adminprod").Visible = false;
                this.Page.Master.FindControl("adminven").Visible = false;
                Response.Redirect("Productos.aspx");
            }
            if (admin != "")
            {
                this.Page.Master.FindControl("inisesion").Visible = false;
                this.Page.Master.FindControl("cersesion").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("inisesion").Visible = true;
                this.Page.Master.FindControl("cersesion").Visible = false;
            }

            if (!IsPostBack)
            {
                Session["Id_producto"] = null;
                Session["Nombre_producto"] = null;
                Session["Precio_producto"] = null;
                Session["Descripcion_producto"] = null;
                Session["Categoria_producto"] = null;
                Session["Stock_producto"] = null;
                Session["Imagen_producto"] = null;
                GridView2.DataSource=oBLLProducto.Leer();
                GridView2.DataBind();
            }
            else
            {
                GridView2.DataSource = oBLLProducto.Leer();
                GridView2.DataBind();
            }

        }


        protected void btnAlta_Click(object sender, EventArgs e)
        {
            Response.Redirect("/AltaProd.aspx");
            
        }

        protected void btnAlta_Click1(object sender, EventArgs e)
        {
            Response.Redirect("/AltaProd.aspx");
        }

        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["Id_producto"] = this.GridView2.Rows[GridView2.SelectedIndex].Cells[1].Text;
            Session["Nombre_producto"] = this.GridView2.Rows[GridView2.SelectedIndex].Cells[2].Text;
            Session["Precio_producto"] = this.GridView2.Rows[GridView2.SelectedIndex].Cells[3].Text;
            Session["Descripcion_producto"] = this.GridView2.Rows[GridView2.SelectedIndex].Cells[4].Text;
            Session["Categoria_producto"] = this.GridView2.Rows[GridView2.SelectedIndex].Cells[5].Text;
            Session["Stock_producto"] = this.GridView2.Rows[GridView2.SelectedIndex].Cells[6].Text;
            Session["Imagen_producto"] = this.GridView2.Rows[GridView2.SelectedIndex].Cells[7].Text;
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            if (Session["Id_Producto"]!=null)
            {
                Response.Redirect("/ModifProds.aspx");
            }
            else
            {
                //error no se seleccionó producto para modificar
            }
        }

        protected void btnBaja_Click(object sender, EventArgs e)
        {
            if (Session["Id_Producto"] != null)
            {
                Response.Redirect("/BajaProds.aspx");
            }
            else
            {
                //error no se seleccionó producto para dar de baja
            }

        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            //hacer validaciones para el textbox, se tiene que buscar como %producto%
            
            GridView2.DataSource= oBLLProducto.BuscarProducto(txtProd.Text);
            GridView2.DataBind();
        }
    }
}