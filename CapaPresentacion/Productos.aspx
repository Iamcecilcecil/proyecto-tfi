﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="Productos.aspx.cs" Inherits="CapaPresentacion.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            margin-right: 2px;
        }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="display: flex; justify-content: center; align-items: center; flex-direction: column;">
        <h1>
            Bienvenidos a la Tienda Online de GreenTech</h1>
        &nbsp;<img src="img/images.png" alt="Imagen de GreenTech" style="width: 468px; margin-left: 160px; background-image: none; background-repeat: repeat; height: 144px;" /></div>
<br />
    <asp:GridView ID="GridView1" width="100%" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:CommandField ShowSelectButton="True" ButtonType="Button" SelectText="Agregar a lista de deseos" />
            <asp:TemplateField HeaderText="Catalogo de Productos">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" CssClass="auto-style1" Height="300px" ImageAlign="Right" ImageUrl='<%# Eval("Imagen") %>' Width="300px" />
                    <br />
                    <br />
                    <br />
                    Codigo:<asp:Label ID="lblCodigo" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                    <br />
                    Producto:<asp:Label ID="lblNombre" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                    <br />
                    Precio:<asp:Label ID="lblPrecio" runat="server" Text='<%# Eval("Precio") %>'></asp:Label>
                    <br />
                    Descripcion:<asp:Label ID="lblDesc" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
                    <br />
                    Categoria:<asp:Label ID="lblCat" runat="server" Text='<%# Eval("Categoria") %>'></asp:Label>
                    <br />
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" Font-Size="Large" />
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#7C6F57" />
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#E3EAEB" />
        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F8FAFA" />
        <SortedAscendingHeaderStyle BackColor="#246B61" />
        <SortedDescendingCellStyle BackColor="#D4DFE1" />
        <SortedDescendingHeaderStyle BackColor="#15524A" />
    </asp:GridView>
<br />
    <br />
    </asp:Content>
