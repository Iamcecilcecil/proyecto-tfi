﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CapaPresentacion
{
    public partial class PreguntasFrecuentes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string admin = Convert.ToString(Session["esadmin"]);
            if (admin == "1")
            {
                this.Page.Master.FindControl("admincup").Visible = true;
                this.Page.Master.FindControl("adminprod").Visible = true;
                this.Page.Master.FindControl("adminven").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("admincup").Visible = false;
                this.Page.Master.FindControl("adminprod").Visible = false;
                this.Page.Master.FindControl("adminven").Visible = false;
            }
            if (admin != "")
            {
                this.Page.Master.FindControl("inisesion").Visible = false;
                this.Page.Master.FindControl("cersesion").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("inisesion").Visible = true;
                this.Page.Master.FindControl("cersesion").Visible = false;
            }
        }
    }
}