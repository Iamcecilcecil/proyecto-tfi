﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaBE;
using CapaBLL;
using Servicios;


namespace CapaPresentacion
{
    public partial class Registro : System.Web.UI.Page
    {
        BECliente oBECliente = new BECliente();
        BLLCliente oBLLCliente = new BLLCliente();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnRegistrarse_Click(object sender, EventArgs e)
        {
            oBECliente.Nombre = txtNombre.Text;
            oBECliente.Apellido = txtApellido.Text;
            oBECliente.Email = txtMail.Text;

            if (txtContraseña.Text != txtContraseña2.Text)
            {
                Label1.Text = "Las contraseñas no coinciden, por favor reingresarlas";
            }
            else
            {
                oBECliente.Contraseñia = Encriptador.MD5(txtContraseña.Text);
                oBECliente.Telefono = Convert.ToInt32(txtTelefono.Text);

                oBLLCliente.Validacion_user_pass(oBECliente);
                oBLLCliente.Alta(oBECliente);
                Label1.Text = "Usuario registrado correctamente";

                LimpiarCampos();
            }

        }
        private void LimpiarCampos()
        {
            txtNombre.Text = "";
            txtApellido.Text = "";
            txtMail.Text = "";
            txtTelefono.Text = "";
            txtContraseña.Text = "";
        }
    }

}
