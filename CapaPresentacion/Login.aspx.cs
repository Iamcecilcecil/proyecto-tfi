﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaBE;
using CapaBLL;
using Servicios;

namespace CapaPresentacion
{
    public partial class Log2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            
        }
        public string result;
        protected void btnIniciarSesion_Click(object sender, EventArgs e)
        {
            BECliente oBEcliente = new BECliente();
            BLLCliente oBLLcliente = new BLLCliente();

            oBEcliente.Email = txtUsuario.Text;
            oBEcliente = oBLLcliente.ListarCliente(oBEcliente);

            if (oBEcliente.Nombre == null && oBEcliente.Apellido == null)
            {
                lblError.Text = "Email no registrado en el sistema";
            }
            else
            {
                result = Encriptador.MD5(txtContraseña.Text);

                //Cliente
                if (oBEcliente.Contraseñia == result && oBEcliente.Permiso == 0)
                {
                    Session["esadmin"] = 0;
                    Response.Redirect("/Productos.aspx");
                }
                else
                { //Usuario admin
                    if (oBEcliente.Contraseñia == result && oBEcliente.Permiso == 1)
                    {
                        this.Page.Master.FindControl("admincup").Visible = true;
                        this.Page.Master.FindControl("adminprod").Visible = true;
                        this.Page.Master.FindControl("adminven").Visible = true;
                        Session["esadmin"] = "1"; //guardo que es administrador en una variable de sesion
                        Response.Redirect("/Productos.aspx");
                    }
                    else
                    {
                        if (oBEcliente.Contraseñia != result)
                        {
                            lblError.Text = "Contraseña incorrecta";
                        }
                    }
                }
            }
        }
    }
}
