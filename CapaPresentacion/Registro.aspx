﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="Registro.aspx.cs" Inherits="CapaPresentacion.Registro" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <body class="bg-black">
    <div class="form-box" id="login-box" style="margin-top:20px">
        <div class="header" style="background-color: #008080">Registro de nuevo usuario</div>
        <div class="body bg-gray">
            <div class="form-group">
                <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" placeholder="Ingrese su nombre" MaxLength="20"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtMail" forecolor="#df1154" ErrorMessage="Por favor, ingrese un Nombre valido"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" forecolor="#df1154" ErrorMessage="Nombre invalido" ControlToValidate="txtNombre" ValidationExpression="^[A-Za-z]{1,20}$"></asp:RegularExpressionValidator>
            </div>
            <div class="form-group">
                <asp:TextBox ID="txtApellido" runat="server"  CssClass="form-control" placeholder="Ingrese su apellido" MaxLength="20"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtApellido" forecolor="#df1154" ErrorMessage="Por favor, ingrese un Apellido valido"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" forecolor="#df1154" ErrorMessage="Apellido invalido" ControlToValidate="txtApellido" ValidationExpression="^[A-Za-z]{1,20}$"></asp:RegularExpressionValidator>
            </div>
             <div class="form-group">
                <asp:TextBox ID="txtTelefono" runat="server"  CssClass="form-control" placeholder="Ingrese su telefono" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtTelefono" forecolor="#df1154" ErrorMessage="Por favor, ingrese un Telefono valido"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" forecolor="#df1154" ErrorMessage="Telefono invalido" ControlToValidate="txtTelefono" ValidationExpression="^[0-9]{10}$"></asp:RegularExpressionValidator>
             </div>
            <div class="form-group">
                <asp:TextBox ID="txtMail" runat="server"  CssClass="form-control" placeholder="Ingrese su correo electronico"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtMail" forecolor="#df1154" ErrorMessage="Por favor, ingrese un email valido"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" forecolor="#df1154" ErrorMessage="Email invalido" ControlToValidate="txtMail" ValidationExpression="^([0-9A-Za-z.\-+]+@[0-9A-Za-z.\-]+\.[A-Za-z]{2,6})$"></asp:RegularExpressionValidator>
            </div>
            <div class="form-group">
                <asp:TextBox ID="txtContraseña" runat="server"  CssClass="form-control" placeholder="Ingrese su contraseña"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:TextBox ID="txtContraseña2" runat="server"  CssClass="form-control" placeholder="Repita su contraseña"></asp:TextBox>
            </div>
        </div>
        <div class="footer">
            <asp:Button ID="btnRegistrarse" runat="server" Text="Registrarse" CssClass="btn bg-till btn-block" Height="50px" BackColor="#008080" OnClick="btnRegistrarse_Click" />
         <asp:Label ID="Label1" CssClass="alert-success" runat="server"></asp:Label>
            <br/>
        <asp:HyperLink ID="HyperLink" runat="server" NavigateUrl="~/Login.aspx">Iniciar sesión</asp:HyperLink>
        </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
</body>

</asp:Content>
