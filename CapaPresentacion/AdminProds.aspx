﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="AdminProds.aspx.cs" Inherits="CapaPresentacion.AdminProds" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <style>
           .grid1{
               margin-left:auto;
               margin-right:auto;
                
           }
           .label
           {
               display:flex;
               justify-content:center;
               align-items:center;
               color:black;
           }

           .control-container{
             
               display:flex;
               margin-right:1px;
           }

            .auto-style1 {
                height: 253px;
            }
            .auto-style2 {
                font-size: large;
                text-align: center;
                text-decoration: underline;
            }
            .button-container
        {
            text-align:justify;
            margin-top:20px;
        }
            .grid1{
               margin-left:auto;
               margin-right:auto;
                
           }
           .label
           {
               display:flex;
               justify-content:center;
               align-items:center;
               color:black;
           }

           .control-container{
             
               display:flex;
               margin-right:1px;
           }
           .gridview-style {
        width: 100%;
        margin: 0 auto;
        border-collapse: collapse;
        text-align: center;
    }

    .gridview-style th {
        background-color: teal;
        color: white;
        font-weight: bold;
        padding: 10px;
    }

    .gridview-style tr {
        background-color: teal;
    }

    .gridview-style .alt {
        background-color: white;
    }

    .gridview-style .selected {
        background-color: #C5BBAF;
        font-weight: bold;
        color: #333333;
    }

    .gridview-style .edit {
        background-color: #7C6F57;
    }

    .gridview-style .footer {
        background-color: #1C5E55;
        color: white;
        font-weight: bold;
    }
        </style>
        <div>
            <asp:Label ID="lbltitulo" runat="server" Text="Administracion de productos" CssClass="label" Font-Names="72 Black" Font-Size="XX-Large"></asp:Label>
            
            <br />
            <br />
            <asp:Label ID="lblBuscar" runat="server" Text="Buscar producto"></asp:Label>
            <div class="control-container">
        <asp:TextBox ID="txtProd" runat="server" CssClass="form-control" Width="200px"></asp:TextBox> 
            &nbsp;&nbsp;&nbsp; 
             <asp:Button ID="btnBuscar" runat="server" CssClass="form-control" Text="Buscar" Height="32px" Width="118px" BackColor="Teal" ForeColor="White" BorderStyle="None" OnClick="btnBuscar_Click"/>                
            <asp:Image ID="Image1" runat="server" Height="100px" ImageUrl="~/img/List-PNG-Transparent-Image.png" Width="96px" Style="float:right; margin-left:300px; margin-right:15px; margin-top:-10px" ImageAlign="Right"/>
            </div>
            <br />
                <div class="auto-style1">
                    <table style="width: 100%;">
                        <tr>
                            <td class="auto-style2"><strong>Listado de Productos<br />
                                </strong></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="button-container">
                                    <br />
                                    <asp:Button ID="btnAlta" runat="server" Text="Alta de producto" Height="50px" Width="150px" BackColor="Teal" ForeColor="White" BorderStyle="None" Style="margin-right:30px; margin-left:130px" OnClick="btnAlta_Click1" />
                                    <asp:Button ID="btnModificar" runat="server" Text="Modificar producto" Height="50px" Width="150px" BackColor="Teal" ForeColor="White" BorderStyle="None" Style="margin-right:30px;" OnClick="btnModificar_Click"/>
                                     <asp:Button ID="btnBaja" runat="server" Text="Baja de producto" Height="50px" Width="150px" BackColor="Teal" ForeColor="White" BorderStyle="None" Style="margin-right:30px;" OnClick="btnBaja_Click" />
                                       
                                    <br />
                                    <br />
                                    <br />
                                 </div>
                               <div>
                                <asp:GridView ID="GridView2" runat="server" CssClass="gridview-style" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False"  OnSelectedIndexChanged="GridView2_SelectedIndexChanged">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True" />
                                        <asp:BoundField DataField="ID" HeaderText="ID_Producto" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" FooterStyle-HorizontalAlign="Center" FooterStyle-VerticalAlign="Middle" >
<FooterStyle HorizontalAlign="Center" VerticalAlign="Middle"></FooterStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" FooterStyle-HorizontalAlign="Center" FooterStyle-VerticalAlign="Middle">
<FooterStyle HorizontalAlign="Center" VerticalAlign="Middle"></FooterStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" FooterStyle-HorizontalAlign="Center" FooterStyle-VerticalAlign="Middle">
<FooterStyle HorizontalAlign="Center" VerticalAlign="Middle"></FooterStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" FooterStyle-HorizontalAlign="Center" FooterStyle-VerticalAlign="Middle">
<FooterStyle HorizontalAlign="Center" VerticalAlign="Middle"></FooterStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Categoria" HeaderText="Categoria" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" FooterStyle-HorizontalAlign="Center" FooterStyle-VerticalAlign="Middle">
<FooterStyle HorizontalAlign="Center" VerticalAlign="Middle"></FooterStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Stock" HeaderText="Stock" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" FooterStyle-HorizontalAlign="Center" FooterStyle-VerticalAlign="Middle" >
<FooterStyle HorizontalAlign="Center" VerticalAlign="Middle"></FooterStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:ImageField DataImageUrlField="Imagen" HeaderText="Imagen" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="50">
<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px"></ItemStyle>
                                        </asp:ImageField>
                                    </Columns>
                                    <EditRowStyle BackColor="#7C6F57" />
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#E3EAEB" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                    </div>
            </div>






</asp:Content>
