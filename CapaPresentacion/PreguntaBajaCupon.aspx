﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="PreguntaBajaCupon.aspx.cs" Inherits="CapaPresentacion.PreguntaBajaCupon" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<body class="bg-black">
    <div class="form-box" id="login-box" style="margin-top:20px">
        <div class="header" style="background-color: #008080">Confirmación</div>
        <div class="body bg-gray">
            <div class="form-group">
                <asp:Label ID="lblPregunta" runat="server" Text="¿Realmente desea dar de baja el cupón?"></asp:Label>
            </div>
        <div class="footer">
        <asp:Button ID="btnBaja" runat="server" Text="Dar de Baja" CssClass="btn bg-till btn-block" Height="50px" BackColor="#008080" OnClick="btnBaja_Click" />
            <br />
        <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn bg-till btn-block" Height="50px" BackColor="#008080" OnClick="btnCancelar_Click" Width="157px" />
            <br />
        <asp:Label runat="server" ID="lblError" CssClass="alert-danger"></asp:Label>
            <br />
        <asp:Label ID="lblOk" CssClass="alert-success" runat="server"></asp:Label>
            <br />
            
        </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
</body>

</asp:Content>
