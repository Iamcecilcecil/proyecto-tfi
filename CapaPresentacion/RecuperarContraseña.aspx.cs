﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaBE;
using Servicios;
using CapaBLL;

namespace CapaPresentacion
{
    public partial class RecuperarContraseña : System.Web.UI.Page
    {
        BECliente oBECliente = new BECliente();
        BLLCliente oBLLCliente= new BLLCliente();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCambiar_Click(object sender, EventArgs e)
        {
            Verificar();

        }

        private void Verificar()
        {
            bool rdo= oBLLCliente.Validacion_usuario(txtUsuario.Text);
            if (rdo==true && txtContraseña.Text==txtContraseña2.Text)
            {
                oBECliente.Email = txtUsuario.Text;
                //valida que el usuario existe y que las contraseñas ingresadas son iguales
                oBECliente.Contraseñia = Encriptador.MD5(txtContraseña.Text);
                oBLLCliente.ModificarContraseña(oBECliente);
                //agregar validacion de datos igual que en
                //formulario de registro
            }
            {
                //el usuario no existe 
            }

        }
    }
}