﻿using System;
using System.Collections.Generic;
using System.Linq;
using CapaBE;
using CapaBLL;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Servicios;
using System.Web.Services.Description;

namespace CapaPresentacion
{
    public partial class EditarCompra : System.Web.UI.Page
    {
        BECompra oBECompra = new BECompra();
        BLLCompra oBLLCompra = new BLLCompra();
        protected void Page_Load(object sender, EventArgs e)
        {
            string admin = Convert.ToString(Session["esadmin"]);
            if (admin == "1")
            {
                this.Page.Master.FindControl("admincup").Visible = true;
                this.Page.Master.FindControl("adminprod").Visible = true;
                this.Page.Master.FindControl("adminven").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("admincup").Visible = false;
                this.Page.Master.FindControl("adminprod").Visible = false;
                this.Page.Master.FindControl("adminven").Visible = false;
                Response.Redirect("Productos.aspx");
            }

            if (admin != "")
            {
                this.Page.Master.FindControl("inisesion").Visible = false;
                this.Page.Master.FindControl("cersesion").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("inisesion").Visible = true;
                this.Page.Master.FindControl("cersesion").Visible = false;
            }

            if (Page.IsPostBack == true)
            {

            }
            else
            {
                oBECompra = (BECompra)Session["compra"];
                txtCodigo.Text = oBECompra.ID.ToString();
                txtTotal.Text = oBECompra.Total.ToString();
                DropEstado.Text = oBECompra.Estado;
                txtComentario.Text = oBECompra.Comentario;
                txtDireccion.Text = oBECompra.Direccion;
                txtAltura.Text = oBECompra.Altura.ToString();
                txtPais.Text = oBECompra.Pais;
                txtProvincia.Text = oBECompra.Provincia;
                txtCiudad.Text = oBECompra.Ciudad;
                txtCP.Text = oBECompra.CP.ToString();
                txtCliente.Text = oBECompra.Cliente.Nombre + " " + oBECompra.Cliente.Apellido;
            }
        }

        protected void btnAlta_Click(object sender, EventArgs e)
        {
            oBECompra = (BECompra)Session["compra"];
            oBECompra.ID = Convert.ToInt32(txtCodigo.Text);
            oBECompra.Estado = DropEstado.SelectedValue.ToUpper();
            oBECompra.Comentario = txtComentario.Text;
            bool rdo = oBLLCompra.ModificarEstado(oBECompra);

            if (rdo == true)
            {
                lblOk.Text = "Compra modificada correctamente";
                if (oBECompra.Estado == "ENVIADO")
                {
                    EnviarMailAlUsuario();
                }
            }
            else
            {
                lblError.Text = "Ha ocurrido un error";
            }
            LimpiarControles();
            LimpiarVariablesSesion();
            Response.Redirect("/AdminCompras.aspx");
        }

        private void LimpiarControles()
        {
            txtCodigo.Text = "";
            txtTotal.Text = "";
            DropEstado.Text = "";
            txtComentario.Text = "";
            txtDireccion.Text = "";
            txtAltura.Text = "";
            txtPais.Text = "";
            txtProvincia.Text = "";
            txtCiudad.Text = "";
            txtCP.Text = "";
            txtCliente.Text = "";
        }
        
        private void LimpiarVariablesSesion()
        {
            Session["compra"] = null;
        }
        private void EnviarMailAlUsuario()
        {
            Correo objcorreo = new Correo(oBECompra.Cliente.Email, "Su pedido ha sido ENVIADO ", "Nro de pedido:" + oBECompra.ID + "\nDetalle: " + "Su pedido ha cambiado de estado a ENVIADO, lo recibiras en las proximas 72 hs :) \n\n Saludos, Equipo GreenTech");

            if (objcorreo.Estado)
            {
                lblOk.Text = "enviado ok";
            }
            else

            {
                lblOk.Text = objcorreo.Merror.ToString();
            }
        }
    }
}