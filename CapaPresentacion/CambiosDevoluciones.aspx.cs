﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Servicios;

namespace CapaPresentacion
{
    public partial class CambiosDevoluciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string admin = Convert.ToString(Session["esadmin"]);
            if(admin=="1")
            {
                this.Page.Master.FindControl("admincup").Visible = true;
                this.Page.Master.FindControl("adminprod").Visible = true;
                this.Page.Master.FindControl("adminven").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("admincup").Visible = false;
                this.Page.Master.FindControl("adminprod").Visible = false;
                this.Page.Master.FindControl("adminven").Visible = false;
                Response.Redirect("Productos.aspx");
            }
            if (admin != "")
            {
                this.Page.Master.FindControl("inisesion").Visible = false;
                this.Page.Master.FindControl("cersesion").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("inisesion").Visible = true;
                this.Page.Master.FindControl("cersesion").Visible = false;
            }

           
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            string nombre = txtNombre.Text;
            string apellido = txtApellido.Text;
            string email= txtEmail.Text;
            string telefono = txtTelefono.Text;
            string nro_pedido = txtPedido.Text;
            string motivo = DropDownList1.Text;
            string mensaje = txtMensaje.Text;

            Correo objcorreo = new Correo("ceciliamodolo@gmail.com", nombre +" "+ apellido+ " " + email, "Nro de pedido:" + nro_pedido + "\nMotivo: " + motivo + "\nMensaje: " + mensaje);

            if (objcorreo.Estado)
            {
                lblOk.Text= "enviado ok";
            }
            else

            {
                lblOk.Text= objcorreo.Merror.ToString();
            }
        }
    }
}