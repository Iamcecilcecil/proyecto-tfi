﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="BajaProds.aspx.cs" Inherits="CapaPresentacion.BajaProds" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<body class="bg-black">
    <div class="form-box" id="login-box" style="margin-top:20px">
        <div class="header" style="background-color: #008080">Baja de Producto</div>
        <div class="body bg-gray">
            <div class="form-group">
                <asp:Label ID="lblId" runat="server" Text="Id de Producto"></asp:Label>
                <asp:TextBox ID="txtId" runat="server" CssClass="form-control" Enabled="False" ></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblNombre" runat="server" Text="Nombre de Producto"></asp:Label>
                <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" ></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblPrecio" runat="server" Text="Precio"></asp:Label>
                <asp:TextBox ID="txtPrecio" runat="server"  CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblDescripcion" runat="server" Text="Descripcion"></asp:Label>
                <asp:TextBox ID="txtDescripcion" runat="server"  CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblCategoria" runat="server" Text="Categoria"></asp:Label>
                <asp:TextBox ID="txtCategoria" runat="server"  CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblStock" runat="server" Text="Stock"></asp:Label>
                <asp:TextBox ID="txtStock" runat="server"  CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblImagen" runat="server" Text="Imagen"></asp:Label>
                <asp:TextBox ID="txtImagen" runat="server"  CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="footer">
        <asp:Button ID="btnBaja" runat="server" Text="Dar de Baja" CssClass="btn bg-till btn-block" Height="50px" BackColor="#008080" OnClick="btnBaja_Click" />
            <br />
        <asp:Label runat="server" ID="lblError" CssClass="alert-danger"></asp:Label>
            <br />
        <asp:Label ID="lblOk" CssClass="alert-success" runat="server"></asp:Label>
            <br />
            
        </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
</body>

</asp:Content>