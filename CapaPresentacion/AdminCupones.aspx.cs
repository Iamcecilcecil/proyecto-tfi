﻿using CapaBE;
using CapaBLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CapaPresentacion
{
    public partial class AdminCupones1 : System.Web.UI.Page
    {
        BECupon oBECupon = new BECupon();
        BLLCupon oBLLCupon= new BLLCupon();
        protected void Page_Load(object sender, EventArgs e)
        {
            string admin = Convert.ToString(Session["esadmin"]);
            if (admin == "1")
            {
                this.Page.Master.FindControl("admincup").Visible = true;
                this.Page.Master.FindControl("adminprod").Visible = true;
                this.Page.Master.FindControl("adminven").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("admincup").Visible = false;
                this.Page.Master.FindControl("adminprod").Visible = false;
                this.Page.Master.FindControl("adminven").Visible = false;
                Response.Redirect("Productos.aspx");
            }

            if (admin != "")
            {
                this.Page.Master.FindControl("inisesion").Visible = false;
                this.Page.Master.FindControl("cersesion").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("inisesion").Visible = true;
                this.Page.Master.FindControl("cersesion").Visible = false;
            }

            if (!IsPostBack)
            {
                Session["Id_cupon"] = null;
                Session["Codigo_cupon"] = null;
                Session["Estado_cupon"] = null;
                Session["Fecha_cupon"] = null;
                Session["Porcentaje_cupon"] = null;
                GridView1.DataSource = oBLLCupon.Leer();
                GridView1.DataBind();
            }
            else
            {
                GridView1.DataSource = oBLLCupon.Leer();
                GridView1.DataBind();
            }
            GridView1.DataSource = oBLLCupon.Leer();
            GridView1.DataBind();
        }
       

        // Función auxiliar para obtener el índice de una columna por su nombre
        private int GetColumnIndexByName(GridView grid, string columnName)
        {
            for (int i = 0; i < grid.HeaderRow.Cells.Count; i++)
            {
                if (grid.HeaderRow.Cells[i].Text.ToLower() == columnName.ToLower())
                {
                    return i;
                }
            }
            return -1;
        }

        protected void btnAlta_Click(object sender, EventArgs e)
        {
            Response.Redirect("/AltaCupones.aspx");
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["Id_cupon"] = this.GridView1.Rows[GridView1.SelectedIndex].Cells[1].Text;
            Session["Codigo_cupon"] = this.GridView1.Rows[GridView1.SelectedIndex].Cells[2].Text;
            Session["Estado_cupon"]= this.GridView1.Rows[GridView1.SelectedIndex].Cells[3].Text;
            Session["Fecha_cupon"] = this.GridView1.Rows[GridView1.SelectedIndex].Cells[4].Text;
            Session["Porcentaje_cupon"]= this.GridView1.Rows[GridView1.SelectedIndex].Cells[5].Text;

        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            if (Session["Id_cupon"] != null)
             {
                Response.Redirect("/ModifCupon.aspx");
            }
            else
            {
                //error no se selecciono un cupon
            }
           
        }

        protected void btnBaja_Click(object sender, EventArgs e)
        {
            if (Session["Id_cupon"] != null)
            {
                Response.Redirect("/BajaCupon.aspx");
            }
            else
            {
                //error no se selecciono u cupon
            }    
        }
    }
}