﻿using CapaBLL;
using System;
using CapaBE;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CapaPresentacion
{
    public partial class ListaDeseos1 : System.Web.UI.Page
    {
        BECupon oBECupon = new BECupon();
        BLLCupon oBLLCupon = new BLLCupon();
        BEListaDeseos oBEListaDeseos= new BEListaDeseos();
        BLLListaDeseos oBLLListaDeseos= new BLLListaDeseos();

        protected void Page_Load(object sender, EventArgs e)
        {
            string admin = Convert.ToString(Session["esadmin"]);
            if (admin == "1")
            {
                this.Page.Master.FindControl("admincup").Visible = true;
                this.Page.Master.FindControl("adminprod").Visible = true;
                this.Page.Master.FindControl("adminven").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("admincup").Visible = false;
                this.Page.Master.FindControl("adminprod").Visible = false;
                this.Page.Master.FindControl("adminven").Visible = false;
            }

            if (admin != "")
            {
                this.Page.Master.FindControl("inisesion").Visible = false;
                this.Page.Master.FindControl("cersesion").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("inisesion").Visible = true;
                this.Page.Master.FindControl("cersesion").Visible = false;
            }



            GridView1.DataSource = Session["listaproductos"];
            GridView1.DataBind();
            if (Session["listaproductos"] == null)
            {
                Label1.Text = "Su lista de deseos se encuentra vacia";
            }
        }

        protected void btnContinuar_Click(object sender, EventArgs e)
        {
            Response.Redirect("Productos.aspx");
        }

        protected void btnComprar_Click(object sender, EventArgs e)
        {
            //validar que no se haya tocado comprar estando todo vacio
            oBEListaDeseos.ID = 0;
            oBEListaDeseos.Productos = (List<BEProducto>)Session["listaproductos"];
            oBEListaDeseos.ID = oBLLListaDeseos.GenerarNroCarrito();

            Dictionary<int, int> recuentoPorID = new Dictionary<int, int>();

            foreach (BEProducto prod in oBEListaDeseos.Productos)
            {
                
                int codigo = prod.ID;

                if (recuentoPorID.ContainsKey(codigo))
                {
                    // Si existe, aumenta el recuento en 1
                    recuentoPorID[codigo]++;
                }
                else
                {
                    // Si no existe, agrega el ID al diccionario con un recuento de 1
                    recuentoPorID[codigo] = 1;
                }

            }

            oBLLListaDeseos.GuardarProductos(oBEListaDeseos.ID, recuentoPorID);

            //cuando vengo a comprar, primero genero el nro de carrito.
            // despues voy a calcular los productos y las cantidades para insertarlos en 
            //la bd
            //en la tabla producto_listadeseos
            //Luego avanzo a la pagina de compra TO DO 
            //en la pagina de compra voy a tener un resumen de la compra
            // voy a tener la opcion de agregar el cupon de pago
            // o continuar la compra y finalizarla
        }
    }
}