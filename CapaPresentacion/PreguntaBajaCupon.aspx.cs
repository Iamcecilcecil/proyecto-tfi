﻿using CapaBE;
using CapaBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CapaPresentacion
{
    public partial class PreguntaBajaCupon : System.Web.UI.Page
    {
        int id;
        BECupon oBECupon = new BECupon();
        BLLCupon oBLLCupon = new BLLCupon();
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Convert.ToInt32(Session["Id_cupon"]);
            string admin = Convert.ToString(Session["esadmin"]);
            if (admin == "1")
            {
                this.Page.Master.FindControl("admincup").Visible = true;
                this.Page.Master.FindControl("adminprod").Visible = true;
                this.Page.Master.FindControl("adminven").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("admincup").Visible = false;
                this.Page.Master.FindControl("adminprod").Visible = false;
                this.Page.Master.FindControl("adminven").Visible = false;
                Response.Redirect("Productos.aspx");
            }

            if (admin != "")
            {
                this.Page.Master.FindControl("inisesion").Visible = false;
                this.Page.Master.FindControl("cersesion").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("inisesion").Visible = true;
                this.Page.Master.FindControl("cersesion").Visible = false;
            }

            if (Page.IsPostBack == true)
            {

            }
            else
            {

             

            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
           Response.Redirect("/AdminCupones.aspx");
        }

        protected void btnBaja_Click(object sender, EventArgs e)
        {
            
            oBECupon.ID = id;
            oBLLCupon.Baja(oBECupon);
            LimpiarVariablesSesion();
            Response.Redirect("/AdminCupones.aspx");
        }
        private void LimpiarVariablesSesion()
        {
            Session["Id_cupon"] = "";
            Session["Codigo_cupon"] = "";
            Session["Estado_cupon"] = "";
            Session["Fecha_cupon"] = "";
            Session["Porcentaje_cupon"] = "";
        }

    }
}