﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using CapaBE;
using CapaBLL;

namespace CapaPresentacion
{
    public partial class AltaProd : System.Web.UI.Page
    {
        BEProducto oBEProducto = new BEProducto();
        BLLProducto oBLLProducto= new BLLProducto();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCargar_Click(object sender, EventArgs e)
        {


        }

        protected void btnAlta_Click(object sender, EventArgs e)
        {
            oBEProducto.Nombre = txtNombre.Text;
            oBEProducto.Precio = Convert.ToInt32(txtPrecio.Text);
            oBEProducto.Descripcion = txtDescripcion.Text;
            oBEProducto.Categoria = txtCategoria.Text;
            oBEProducto.Stock = Convert.ToInt32(txtStock.Text);
            oBLLProducto.Alta(oBEProducto);
            oBEProducto.ID = oBLLProducto.ObtenerID(oBEProducto);

            ComprobarExistencia();
            if (FileUpload1.HasFile)
            {
                FileUpload1.SaveAs(Server.MapPath("/imagenes/"+ FileUpload1.FileName));
                oBLLProducto.SubirImagen("/imagenes/" + FileUpload1.FileName, oBEProducto.ID);
            }
        }

        private void ComprobarExistencia()
        {
            bool exists = System.IO.Directory.Exists(Server.MapPath("/imagenes/"));
            if (!exists)
            {
                System.IO.Directory.CreateDirectory(Server.MapPath("/imagenes/"));
            }
            else
            {

            }


        }
    }
}