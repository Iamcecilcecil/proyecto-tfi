﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="PreguntasFrecuentes.aspx.cs" Inherits="CapaPresentacion.PreguntasFrecuentes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Preguntas frecuentes</h1>
        
         <div class="form-group">
        <asp:Label ID="lblPregunta1" runat="server" Text="No recibi mi pedido, ¿Que hago?"></asp:Label>
             <br />
             <br />
             <asp:Label ID="lblrta1" runat="server" Text="Por favor comunicate con nosotros llamando o contactándonos por whatsapp al: 11-7078-0855. No olvides tener a mano tu número de pedido."></asp:Label>
             <br />
             <br />
        <asp:Label ID="lblPregunta2" runat="server" Text="¿Qué pasa si no hay nadie cuando traen mi pedido?"></asp:Label>
             <br />
             <br />
             <asp:Label ID="lblrta2" runat="server" Text="Si no hay nadie en el domicilio que nos indicaste, el correo regresará a las 48 horas. En caso de no encontrar a nadie, deberás dirigirte al centro de distribución asignado a tu pedido dentro de las 72 horas con tu DNI y el código que te enviamos (tracking number)."></asp:Label>
             <br />
             <br />
        <asp:Label ID="lblPregunta3" runat="server" Text="¿Puede recibir el paquete otra persona"></asp:Label>
             <br />
             <br />
             <asp:Label ID="lblrta3" runat="server" Text="Sí, no es necesario que esté presente quien realizó la compra. Puede recibirlo cualquier adulto."></asp:Label>
    </div>
    


</asp:Content>
