﻿using CapaBE;
using CapaBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CapaPresentacion
{
    public partial class ModifProds : System.Web.UI.Page
    {
        BEProducto oBEProducto= new BEProducto();
        BLLProducto oBLLProducto = new BLLProducto();
        protected void Page_Load(object sender, EventArgs e)
        {
            string admin = Convert.ToString(Session["esadmin"]);
            if (admin == "1")
            {
                this.Page.Master.FindControl("admincup").Visible = true;
                this.Page.Master.FindControl("adminprod").Visible = true;
                this.Page.Master.FindControl("adminven").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("admincup").Visible = false;
                this.Page.Master.FindControl("adminprod").Visible = false;
                this.Page.Master.FindControl("adminven").Visible = false;
                Response.Redirect("Productos.aspx");
            }

            if (admin != "")
            {
                this.Page.Master.FindControl("inisesion").Visible = false;
                this.Page.Master.FindControl("cersesion").Visible = true;
            }
            else
            {
                this.Page.Master.FindControl("inisesion").Visible = true;
                this.Page.Master.FindControl("cersesion").Visible = false;
            }

            if (Page.IsPostBack == true)
            {

            }
            else
            {
                txtID.Text = Session["Id_producto"].ToString();
                txtNombre.Text = Session["Nombre_producto"].ToString();
                txtPrecio.Text = Session["Precio_producto"].ToString();
                txtDescripcion.Text = Session["Descripcion_producto"].ToString();
                txtCategoria.Text = Session["Categoria_producto"].ToString();
                txtStock.Text = Session["Stock_producto"].ToString();
                txtImagen.Text= Session["Imagen_producto"].ToString();
            }
        }

        protected void btnAlta_Click(object sender, EventArgs e)
        {
            oBEProducto.ID = Convert.ToInt32(Session["Id_producto"]);
            oBEProducto.Nombre = txtNombre.Text;
            oBEProducto.Precio = Convert.ToInt32(txtPrecio.Text);
            oBEProducto.Descripcion = txtDescripcion.Text.ToString();
            oBEProducto.Categoria = txtCategoria.Text;
            oBEProducto.Stock = Convert.ToInt32(txtStock.Text);

            if(Image.HasFile)
            {
                Image.SaveAs(Server.MapPath("/imagenes/" + Image.FileName));
                oBLLProducto.SubirImagen("/imagenes/" + Image.FileName, oBEProducto.ID);
            }
            else
            {
                oBLLProducto.Modificar(oBEProducto);
            }

            lblOk.Text = "Producto modificado correctamente";

            LimpiarControles();
            LimpiarVariablesSesion();
        }

        private void LimpiarControles()
        {
            txtID.Text = "";
            txtNombre.Text = "";
            txtPrecio.Text = "";
            txtDescripcion.Text = "";
            txtCategoria.Text = "";
            txtStock.Text = "";
            txtImagen.Text = "";
        }

        private void LimpiarVariablesSesion()
        {
            Session["Id_producto"] = "";
            Session["Nombre_producto"] = "";
            Session["Precio_producto"] = "";
            Session["Descripcion_producto"] = "";
            Session["Categoria_producto"] = "";
            Session["Stock_producto"] = "";
            Session["Imagen_producto"] = "";
        }
    }
}