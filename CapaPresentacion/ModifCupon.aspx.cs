﻿using CapaBE;
using CapaBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CapaPresentacion
{
    public partial class ModifCupon : System.Web.UI.Page
    {
        BECupon oBECupon = new BECupon();
        BLLCupon oBLLCupon = new BLLCupon();

        protected void Page_Load(object sender, EventArgs e)
        {
            string admin = Convert.ToString(Session["esadmin"]);
            if (admin == "1")
            {
                this.Page.Master.FindControl("admincup").Visible = true;
                this.Page.Master.FindControl("adminprod").Visible = true;
                this.Page.Master.FindControl("adminven").Visible = true;
            }
                else
                {
                    this.Page.Master.FindControl("admincup").Visible = false;
                    this.Page.Master.FindControl("adminprod").Visible = false;
                    this.Page.Master.FindControl("adminven").Visible = false;
                Response.Redirect("Productos.aspx");
            }

            if (admin != "")
            {
                this.Page.Master.FindControl("inisesion").Visible = false;
                this.Page.Master.FindControl("cersesion").Visible = true;
            }
                else
                {
                    this.Page.Master.FindControl("inisesion").Visible = true;
                    this.Page.Master.FindControl("cersesion").Visible = false;
                }

            if (Page.IsPostBack == true)
            {

            }
            else
            {
                txtId.Text = Session["Id_cupon"].ToString();
                txtCodigo.Text = Session["Codigo_cupon"].ToString();
                txtEstado.Text = Session["Estado_cupon"].ToString();
                txtFecha.Text = Session["Fecha_cupon"].ToString();
                txtPorcentaje.Text = Session["Porcentaje_cupon"].ToString();
            }
        }

        protected void btnModif_Click(object sender, EventArgs e)
        {
            oBECupon.ID = Convert.ToInt32(Session["Id_cupon"]);
            oBECupon.Codigo = txtCodigo.Text;
            oBECupon.Estado = txtEstado.Text;
            oBECupon.Fecha_Vencimiento = Convert.ToDateTime(txtFecha.Text);
            oBECupon.Porcentaje_Descuento = Convert.ToInt32(txtPorcentaje.Text);

            oBLLCupon.Modificar(oBECupon);
            lblOk.Text = "Cupon modificado correctamente";

            LimpiarControles();
            LimpiarVariablesSesion();
        }

        private void LimpiarControles()
        {
            txtId.Text = "";
            txtCodigo.Text = "";
            txtEstado.Text = "";
            txtFecha.Text = "";
            txtPorcentaje.Text = "";
        }

        private void LimpiarVariablesSesion()
        {
            Session["Id_cupon"] = "";
            Session["Codigo_cupon"] = ""; ;
            Session["Estado_cupon"] = "";
            Session["Fecha_cupon"] = "";
            Session["Porcentaje_cupon"] = "";
        }
    }
}