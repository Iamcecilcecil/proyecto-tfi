﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="EditarCompra.aspx.cs" Inherits="CapaPresentacion.EditarCompra" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<body class="bg-black">
    <div class="form-box" id="login-box" style="margin-top:20px">
        <div class="header" style="background-color: #008080">Editar Compra</div>
        <div class="body bg-gray">
            <div class="form-group">
                <asp:Label ID="lblCodigo" runat="server" Text="Codigo de Compra"></asp:Label>
                <asp:TextBox ID="txtCodigo" runat="server"  Enabled="false"  CssClass="form-control" ></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblTotal" runat="server"  Text="Total"></asp:Label>
                <asp:TextBox ID="txtTotal" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblEstado" runat="server"  Text="Estado"></asp:Label>
                <br />
                <asp:DropDownList ID="DropEstado" runat="server">
                    <asp:ListItem Value="PAGO APROBADO"></asp:ListItem>
                    <asp:ListItem Value="ESPERANDO PAGO"></asp:ListItem>
                    <asp:ListItem Value="EN PREPARACION"></asp:ListItem>
                    <asp:ListItem Value="ENVIADO"></asp:ListItem>
                    <asp:ListItem Value="FINALIZADA"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form-group">
                <asp:Label ID="lblComentario" runat="server" Text="Comentario"></asp:Label>
                <asp:TextBox ID="txtComentario" runat="server"  Enabled="true"  CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblDireccion" runat="server" Text="Direccion"></asp:Label>
                <asp:TextBox ID="txtDireccion" runat="server"  Enabled="false" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblAltura" runat="server"  Text="Altura"></asp:Label>
                <asp:TextBox ID="txtAltura" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblPais" runat="server" Text="Pais"></asp:Label>
                <asp:TextBox ID="txtPais" runat="server" Enabled="false"  CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblProvincia" runat="server" Text="Provincia"></asp:Label>
                <asp:TextBox ID="txtProvincia" runat="server"  Enabled="false" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblCiudad" runat="server" Text="Ciudad"></asp:Label>
                <asp:TextBox ID="txtCiudad" runat="server"   Enabled="false" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblCP" runat="server" Text="Codigo Postal"></asp:Label>
                <asp:TextBox ID="txtCP" runat="server"  Enabled="false"  CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblCliente" runat="server" Text="Cliente"></asp:Label>
                <asp:TextBox ID="txtCliente" runat="server"  Enabled="false" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="footer">
        <asp:Button ID="btnEditar" runat="server" Text="Editar" CssClass="btn bg-till btn-block" Height="50px" BackColor="#008080" OnClick="btnAlta_Click" />
        <asp:Label runat="server" ID="lblError" CssClass="alert-danger"></asp:Label>
            <br />
        <asp:Label ID="lblOk" CssClass="alert-success" runat="server"></asp:Label>
            <br />
            
        </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
</body>

</asp:Content>
