﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="AdminCupones.aspx.cs" Inherits="CapaPresentacion.AdminCupones1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <style>
           .grid1{
               margin-left:auto;
               margin-right:auto;
                
           }
           .label
           {
               display:flex;
               justify-content:center;
               align-items:center;
               color:black;
           }

           .control-container{
             
               display:flex;
               margin-right:1px;
           }
           .gridview-style {
        width: 100%;
        margin: 0 auto;
        border-collapse: collapse;
        text-align: center;
    }

    .gridview-style th {
        background-color: teal;
        color: white;
        font-weight: bold;
        padding: 10px;
    }

    .gridview-style tr {
        background-color: teal;
    }

    .gridview-style .alt {
        background-color: white;
    }

    .gridview-style .selected {
        background-color: #C5BBAF;
        font-weight: bold;
        color: #333333;
    }

    .gridview-style .edit {
        background-color: #7C6F57;
    }

    .gridview-style .footer {
        background-color: #1C5E55;
        color: white;
        font-weight: bold;
    }
         
        </style>
        <div>
            <asp:Label ID="lbltitulo" runat="server" Text="Administracion de cupones" CssClass="label" Font-Names="72 Black" Font-Size="XX-Large"></asp:Label>
            <asp:Image ID="Image1" runat="server" Height="98px" ImageUrl="~/img/cupones.png" Width="139px" ImageAlign="Right" Style="float:right"/>
            <br />
            <br />
            <asp:Label ID="lblBuscar" runat="server" Text="Buscar cupón"></asp:Label>
            <div class="control-container">
        <asp:TextBox ID="txtCupon" runat="server" CssClass="form-control" Width="200px"></asp:TextBox> 
            &nbsp;&nbsp;&nbsp; 
             <asp:Button ID="btnBuscar" runat="server" CssClass="form-control" Text="Buscar" Height="32px" Width="118px" BackColor="Teal" ForeColor="White" BorderStyle="None"/>

             
                <br />

             
             </div>
                <br />
                <br />

          <asp:GridView ID="GridView1" runat="server" CssClass="gridview-style" CellPadding="4" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
             <AlternatingRowStyle CssClass="alt" />  
            <columns>
                <asp:CommandField ShowSelectButton="True" />

            </columns>

                <EditRowStyle BackColor="#7C6F57" />
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#E3EAEB" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />

            </asp:GridView>

            <br />
            <br />
            <br />
            <br />
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <asp:Button ID="btnAlta" runat="server" Text="Alta de cupón" Height="50px" Width="150px" BackColor="Teal" ForeColor="White" BorderStyle="None" OnClick="btnAlta_Click"/>

             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <asp:Button ID="btnModificar" runat="server" Text="Modificar cupón" Height="50px" Width="150px" BackColor="Teal" ForeColor="White" BorderStyle="None" OnClick="btnModificar_Click"/>

             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <asp:Button ID="btnBaja" runat="server" Text="Dar de baja cupón" Height="50px" Width="150px" BackColor="Teal" ForeColor="White" BorderStyle="None" OnClick="btnBaja_Click"/>

             <br />
            <br />
        </div>



</asp:Content>
