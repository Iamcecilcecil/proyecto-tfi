﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="RecuperarContraseña.aspx.cs" Inherits="CapaPresentacion.RecuperarContraseña" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!DOCTYPE html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Recuperar contraseña</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="css/AdminLTE.css"/ rel="stylesheet" type="text/css" />
<body class="bg-black">
    <div class="form-box" id="login-box" style="margin-top:20px">
        <div class="header" style="background-color: #008080">Recuperar contraseña</div>
        <div class="body bg-gray">
            <div class="form-group">
                <asp:TextBox ID="txtUsuario" runat="server" CssClass="form-control" placeholder="Ingrese su usuario" ></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:TextBox ID="txtContraseña" runat="server"  CssClass="form-control" placeholder="Ingrese su nueva contraseña"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:TextBox ID="txtContraseña2" runat="server"  CssClass="form-control" placeholder="Repita su nueva contraseña"></asp:TextBox>
            </div>
        </div>
        <div class="footer">
            <asp:Button ID="btnCambiar" runat="server" Text="Cambiar contraseña" CssClass="btn bg-till btn-block" Height="50px" BackColor="#008080" OnClick="btnCambiar_Click" />
        <br/>
        <asp:HyperLink ID="HyperLink" runat="server" NavigateUrl="~/Login.aspx">Iniciar sesión</asp:HyperLink>
        </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>

</asp:Content>
