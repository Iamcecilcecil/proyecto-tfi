﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="ListaDeseos.aspx.cs" Inherits="CapaPresentacion.ListaDeseos1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            text-align: center;
        }
    </style>
    <style>
           .grid1{
               margin-left:auto;
               margin-right:auto;
                
           }
           .label
           {
               display:flex;
               justify-content:center;
               align-items:center;
               color:black;
           }

           .control-container{
             
               display:flex;
               margin-right:1px;
           }
           .gridview-style {
        width: 100%;
        margin: 0 auto;
        border-collapse: collapse;
        text-align: center;
    }

    .gridview-style th {
        background-color: teal;
        color: white;
        font-weight: bold;
        padding: 10px;
    }

    .gridview-style tr {
        background-color: teal;
    }

    .gridview-style .alt {
        background-color: white;
    }

    .gridview-style .selected {
        background-color: #C5BBAF;
        font-weight: bold;
        color: #333333;
    }

    .gridview-style .edit {
        background-color: #7C6F57;
    }

    .gridview-style .footer {
        background-color: #1C5E55;
        color: white;
        font-weight: bold;
    }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Tu lista de deseos</h1>
    <p>
        <asp:Label ID="Label1" runat="server"></asp:Label>
    </p>
        
         <div class="form-group">
              <asp:GridView ID="GridView1" runat="server" CssClass="gridview-style" CellPadding="4" ForeColor="#333333" GridLines="None">
             <AlternatingRowStyle CssClass="alt" />  
            <columns>
                <asp:CommandField ShowSelectButton="True" />

            </columns>

                <EditRowStyle BackColor="#7C6F57" />
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#E3EAEB" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />

            </asp:GridView>
             </div>
             <br />

</div>
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <asp:Button ID="btnContinuar" runat="server" Text="Continuar comprando" Height="50px" Width="141px" BackColor="Teal" ForeColor="White" BorderStyle="None" OnClick="btnContinuar_Click"/>

             <asp:Button ID="btnComprar" runat="server" Text="Comprar" Height="50px" Width="141px" BackColor="Teal" ForeColor="White" BorderStyle="None" OnClick="btnComprar_Click"/>

    <br />
</asp:Content>
