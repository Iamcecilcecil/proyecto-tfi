﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaBE
{
    public class BEProducto
    {
        public int ID { get; set; } 
        public string Nombre { get; set; }
        public int Precio { get; set; }
        public int Stock { get; set; }  
        public string Descripcion { get; set; }
        public string Categoria { get; set; }
        public string Imagen { get; set; }

    }
}
