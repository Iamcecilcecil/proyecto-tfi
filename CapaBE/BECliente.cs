﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaBE
{
    public class BECliente
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Telefono { get; set; }
        public string Email { get; set; }
        public string Contraseñia { get; set; } 
        public int Permiso { get; set; }

        public override string ToString()
        {
            return this.Nombre+ " " + this.Apellido;
        }
    }
   
}
