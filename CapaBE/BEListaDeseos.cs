﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaBE
{
    public class BEListaDeseos
    {
        public int ID { get; set; } 
        public BECliente Cliente { get; set; }
        public BECupon Cupon { get; set; }
        public List<BEProducto> Productos { get; set; }

    }
}
