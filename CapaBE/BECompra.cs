﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaBE
{
    public class BECompra
    {
        public int ID { get;set; }
        public int Total { get; set; }
        public string Estado { get; set; }
        public string Comentario { get; set; }
        public string Direccion { get; set; }
        public int Altura { get;set; }
        public string Pais { get;set; }
        public string Provincia { get;set; }
        public string Ciudad { get; set; }
        public int CP { get; set; }
        public BECliente Cliente { get; set; }
    }
}
