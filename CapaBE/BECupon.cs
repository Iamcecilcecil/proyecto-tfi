﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaBE
{
    public class BECupon
    {
        public int ID { get; set; }
        public string Codigo { get; set; }
        public string Estado {get; set; }
        public DateTime Fecha_Vencimiento { get; set; }
        public int Porcentaje_Descuento { get; set; }

    }
}
