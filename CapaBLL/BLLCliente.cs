﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaBE;
using CapaMPP;

namespace CapaBLL
{
    public class BLLCliente
    {
        MPPCliente oMPPCliente = new MPPCliente();

        public bool Validacion_user_pass(BECliente cliente)
        {
            try
            {
                return oMPPCliente.Validacion_user_pass(cliente);

            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Alta(BECliente oBECliente)
        {
            try
            {
                return oMPPCliente.Alta(oBECliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public bool Validacion_usuario(string usuario)
        {
            try
            {
                return oMPPCliente.Validacion_usuario(usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ModificarContraseña(BECliente oBECliente)
        {
            try
            {
                oMPPCliente.ModificarContraseña(oBECliente);
            }
            catch(Exception ex)
            {

            }
        }
        public BECliente ListarCliente(BECliente oBECliente)
        {
            return oMPPCliente.ListarCliente(oBECliente);

        }
    }
}
