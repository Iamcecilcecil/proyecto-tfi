﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaMPP;
using System.Threading.Tasks;

namespace CapaBLL
{
    public class BLLListaDeseos
    {
        MPPListaDeseos oMPPListaDeseos = new MPPListaDeseos();

        public int GenerarNroCarrito()
        {
            return oMPPListaDeseos.GenerarNroCarrito();
        }

        public void GuardarProductos(int id, Dictionary<int,int> prods)
        {
             oMPPListaDeseos.GuardarProductos(id, prods);
        }
    }
}
