﻿using CapaBE;
using CapaMPP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaMPP;

namespace CapaBLL
{
    public class BLLProducto
    {
        MPPProducto oMPPProducto = new MPPProducto();
        
        public bool Alta(BEProducto oBEProducto)
        {
            try
            {
                return oMPPProducto.Alta(oBEProducto);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public bool Modificar(BEProducto oBEProducto)
        {
            try
            {
                return oMPPProducto.Modificar(oBEProducto);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public IList<BEProducto> Leer()
        {
            try
            {
                return oMPPProducto.Leer();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BEProducto> BuscarProducto(string prod)
        {
            return oMPPProducto.BuscarProducto(prod);
        }
        public int ObtenerID(BEProducto oBEProducto)
        {
            try
            {
                return oMPPProducto.ObtenerID(oBEProducto);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public bool Baja(BEProducto oBEProducto)
        {
            try
            {
                return oMPPProducto.Baja(oBEProducto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SubirImagen( string path, int Id)
        {
            try
            {
                 oMPPProducto.SubirImagen(path, Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
