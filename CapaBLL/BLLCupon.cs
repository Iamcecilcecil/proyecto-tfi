﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaBE;
using CapaMPP;
using System.Threading.Tasks;

namespace CapaBLL
{
    public class BLLCupon
    {
        MPPCupon oMPPCupon = new MPPCupon();

        public bool Alta(BECupon oBECupon)
        {
            try
            {
                return oMPPCupon.Alta(oBECupon);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool Modificar(BECupon oBECupon)
        {
            try
            {
                return oMPPCupon.Modificar(oBECupon);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<BECupon> Leer()
        {
            try
            {
                return oMPPCupon.Leer();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Baja(BECupon oBECupon)
        {
            try
            {
                return oMPPCupon.Baja(oBECupon);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
