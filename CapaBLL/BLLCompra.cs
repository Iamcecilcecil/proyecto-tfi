﻿using CapaBE;
using CapaMPP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaBLL
{
    public class BLLCompra
    {
        MPPCompra oMPPCompra = new MPPCompra();
        public IList<BECompra> Leer()
        {
            try
            {
                return oMPPCompra.Leer();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ModificarEstado(BECompra oBECompra)
        {
            return oMPPCompra.ModificarEstado(oBECompra);
        }
    }
}
